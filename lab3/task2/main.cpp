/* comp2012 2015S Lab03 */

#include <qapplication.h>

#include "MyWidget.h"

int main(int argc, char* argv[])
{
   QApplication app(argc, argv);

   // Add your code for the following 3 steps:
   // 1. create a MyWidget object
   MyWidget *widget = new MyWidget();

   // 2. Set the MyWidget object as the main UI component
   app.setMainWidget(widget);

   // 3. Let the system to show the MyWidget object in the application
   widget->show();

   // enter the window event loop
   return app.exec();
};

