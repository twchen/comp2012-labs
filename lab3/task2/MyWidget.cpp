/* COMP2012 2015S Lab03 */

#include "MyWidget.h"

MyWidget::MyWidget(QWidget* parent, const char* name)
    : QWidget(parent, name)
{

   // set the minimum and maximum size of the widget
   setMinimumSize(240, 120);
   setMaximumSize(480, 240);

   // create the label
   label = new QLabel("Hello!", this, 0);
   // set the location and size of the label
   label->setGeometry(70, 20, 200, 50);

   // create the button
   button = new QPushButton("Click Me", this, 0);
   // set the location and size of the button
   button->setGeometry(70, 70, 100, 50);

   // Add your code to connect the clicked() signal of button with the changeLabelText() slot of this MyWidget object
   QObject::connect(button, SIGNAL(clicked()), this, SLOT(changeLabelText()));

}

void MyWidget::changeLabelText()
{
   // change the text of the label
   label->setText("Button clicked");
}

