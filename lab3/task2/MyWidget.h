#include <qapplication.h>
#include <qpushbutton.h>
#include <qlabel.h>

#ifndef _MY_WIDGET_H
#define _MY_WIDGET_H

class MyWidget: public QWidget {
   // All classes that contain signals and slots must mention Q_OBJECT in their declaration
   Q_OBJECT

   public:
      MyWidget(QWidget* parent = NULL, const char *name = NULL);

   public slots:
      void changeLabelText();

   private:
      QPushButton* button;
      QLabel* label;
};

#endif

