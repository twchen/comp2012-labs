/* COMP2012 2015S Lab03 */

// Prism.h is the header file for the class Prism

#include "Polygon.h"

class Prism: public Polygon 
{
   public:
      Prism();  // Default constructor
      Prism(const Point points[], int numPoints, int height); // Constructor
      Prism(const Prism& p);  // Copy constructor: deep copy required
      ~Prism();  // Destructor

     void print() const; // to print the Polygon base and the height of the Prism object

   private:
     int height; // the height of the Prism object

};

