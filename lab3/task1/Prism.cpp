#include <iostream>
#include "Prism.h"
using namespace std;

// Constructors
Prism::Prism():height(0){
	cout << "Initialized by Prism's default constructor" << endl;
}

Prism::Prism(const Point points[], int numPoints, int height): Polygon(points, numPoints), height(height){// the parameter has the same name as the data member height, is it ok?
	cout << "Initialized by Prism's parameterized constructor" << endl;
}

Prism::Prism(const Prism &p): Polygon(p), height(p.height){// Slicing
	cout << "Initialized by Prism's copy constructor" << endl;
}

// Destructor
Prism::~Prism(){
	cout << "Prism's destructor" << endl;
}

// Member function
void Prism::print() const {
	Polygon::print();
	cout << "Height: " << height << endl;
}

