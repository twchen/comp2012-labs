#ifndef PLANT_H
#define PLANT_H

#include "Organism.h"

class Plant:public Organism
{
public:
	Plant(int w, double p);
	virtual double nutritionValue() = 0;
	void photosynthesis(double percentage);
	int getWeight();
	void setWeight(int w);

private:
	int weight;
};

#endif
