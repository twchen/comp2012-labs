#ifndef ANIMAL_H
#define ANIMAL_H

#include "Organism.h"

class Animal:public Organism
{
public:
	Animal(double p);
	void eat(Organism *org);
	virtual double nutritionValue();
};

#endif
