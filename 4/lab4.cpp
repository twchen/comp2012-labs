#include <iostream>
using namespace std;

class Organism
{
public:
	Organism(double p): lifePoint(p) {};
	void setLifePoint(double p) { lifePoint = p; }
	double getLifePoint() const { return lifePoint; }
	virtual double nutritionValue() const = 0;
	virtual ~Organism() {}; // {} is necessary because virtual function must have definition
	
protected:
	double lifePoint;
};
	

class Animal:public Organism
{
public:
	Animal(double p): Organism(p) {};
	void eat(Organism *victim) { lifePoint += victim->nutritionValue(); }
	virtual double nutritionValue() const { return lifePoint; }
	virtual ~Animal() {};
};


class Plant:public Organism
{
public:
	Plant(int w, double p): Organism(p), weight(w) {};
	virtual double nutritionValue() const = 0;
	void photosynthesis(double percentage) { weight = static_cast<int>( weight * ( 1 + percentage / 100 ) ); }
	virtual ~Plant() {};

protected:
	int weight;
};


class HealthyPlant:public Plant
{
public:
	HealthyPlant(int w, double p): Plant(w, p) {};
	virtual double nutritionValue() const { return weight * lifePoint; }
	virtual ~HealthyPlant() {};
};


class PoisonPlant:public Plant
{
public:
	PoisonPlant(double d, int w, double p): Plant(w, p), degreeOfPoison(d) {};
	virtual double nutritionValue() const { return -1 * degreeOfPoison * weight * lifePoint; }
	virtual ~PoisonPlant() {};

protected:
	double degreeOfPoison;
};


int main() { 
    const int numOfVictim = 3;
 
    // Dynamically allocate an array of Organism* 
    Organism **orgVec = new Organism*[numOfVictim]; 
 
    // Create different organisms 
    //  Predator: crazyMonkey 
    //  Victims: banana, opium and babyMonkey
 
    // crazyMonkey (life point = 110.0) 
    Animal *crazyMonkey = new Animal(110.0); 
 
    // banana (weight = 5 units, life point = 10.0) 
    HealthyPlant *banana = new HealthyPlant(5, 10.0); 
 
    // opium (degree of posion = 2.5 units, weight = 10 units, life point = 6.0) 
    PoisonPlant *opium = new PoisonPlant(2.5, 10, 6.0); 
 
    // babyMonkey (life point = 10.0) 
    Animal *babyMonkey = new Animal(10.0); 
 
    // Conduct photosynthesis: +10% weight
    opium->photosynthesis(10.0); 
 
    orgVec[0] = banana;      // banana is an organism
    orgVec[1] = opium;       // opium is an organism
    orgVec[2] = babyMonkey;  // babyMonkey is an organism
 
    // CrazyMonkey eats all organisms in a loop
    cout << "CrazyMonkey's initial life point : " << crazyMonkey->getLifePoint() << endl;
    for (int i=0; i<numOfVictim; i++) { 
		crazyMonkey->eat(orgVec[i]); 
		cout << "CrazyMoney gains " 
			 << orgVec[i]->nutritionValue() 
			 << " life point " << endl; 
		delete orgVec[i]; 
    } 
    cout << "CrazyMonkey's final life point: " << crazyMonkey->getLifePoint() << endl ; 
 
    // Clean up 
    delete crazyMonkey; 
    delete [] orgVec; 
 
    return 0;
} 
