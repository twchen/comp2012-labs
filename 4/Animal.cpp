#include "Animal.h"

Animal::Animal(double p):Organism(p)
{
}

void Animal::eat(Organism *org)
{
	setLifePoint(getLifePoint() + org->nutritionValue());
}

double Animal::nutritionValue()
{
	return getLifePoint();
}
