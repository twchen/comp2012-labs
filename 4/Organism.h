#ifndef ORGANISM_H
#define ORGANISM_H

class Organism
{
public:
	Organism(double p);
	void setLifePoint(double p);
	double getLifePoint();
	virtual double nutritionValue() = 0;
	
private:
	double lifePoint;
};
	
#endif
