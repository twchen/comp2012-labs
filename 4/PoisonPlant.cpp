#include "PoisonPlant.h"

PoisonPlant::PoisonPlant(double d, int w, double p):Plant(w, p), degreeOfPoison(d)
{
}

double PoisonPlant::nutritionValue()
{
	return -1 * degreeOfPoison * getWeight() * getLifePoint();
}
