#include "Plant.h"

Plant::Plant(int w, double p):Organism(p), weight(w)
{
}

void Plant::photosynthesis(double percentage)
{
	weight = static_cast<int>(weight * ( 1 + percentage / 100 ));
}

int Plant::getWeight()
{
	return weight;
}

void Plant::setWeight(int w)
{
	weight = w;
}
