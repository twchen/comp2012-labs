#ifndef POISONPLANT_H
#define POISONPLANT_H

#include "Plant.h"

class PoisonPlant:public Plant
{
public:
	PoisonPlant(double d, int w, double p);
	virtual double nutritionValue();

private:
	double degreeOfPoison;
};

#endif
