#ifndef HEALTHYPLANT_H
#define HEALTHYPLANT_H

#include "Plant.h"

class HealthyPlant:public Plant
{
public:
	HealthyPlant(int w, double p);
	virtual double nutritionValue();
};

#endif
