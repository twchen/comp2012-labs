#include "HealthyPlant.h"

HealthyPlant::HealthyPlant(int w, double p):Plant(w, p)
{
}

double HealthyPlant::nutritionValue()
{
	return getWeight() * getLifePoint();
}
