#include "Organism.h"

Organism::Organism(double p):lifePoint(p)
{
}

void Organism::setLifePoint(double p)
{
	lifePoint = p;
}

double Organism::getLifePoint()
{
	return lifePoint;
}
