#include "Organism.h" 
#include "HealthyPlant.h" 
#include "PoisonPlant.h" 
#include "Animal.h" 
#include <iostream> 
using namespace std;
 
int main() { 
    const int numOfVictim = 3;
 
    // Dynamically allocate an array of Organism* 
    Organism **orgVec = new Organism*[numOfVictim]; 
 
    // Create different organisms 
    //  Predator: crazyMonkey 
    //  Victims: banana, opium and babyMonkey
 
    // crazyMonkey (life point = 110.0) 
    Animal *crazyMonkey = new Animal(110.0); 
 
    // banana (weight = 5 units, life point = 10.0) 
    HealthyPlant *banana = new HealthyPlant(5, 10.0); 
 
    // opium (degree of posion = 2.5 units, weight = 10 units, life point = 6.0) 
    PoisonPlant *opium = new PoisonPlant(2.5, 10, 6.0); 
 
    // babyMonkey (life point = 10.0) 
    Animal *babyMonkey = new Animal(10.0); 
 
    // Conduct photosynthesis: +10% weight
    opium->photosynthesis(10.0); 
 
    orgVec[0] = banana;      // banana is an organism
    orgVec[1] = opium;       // opium is an organism
    orgVec[2] = babyMonkey;  // babyMonkey is an organism
 
    // CrazyMonkey eats all organisms in a loop
    cout << "CrazyMonkey's initial life point : " << crazyMonkey->getLifePoint() << endl;
    for (int i=0; i<numOfVictim; i++) { 
		crazyMonkey->eat(orgVec[i]); 
		cout << "CrazyMoney gains " 
			 << orgVec[i]->nutritionValue() 
			 << " life point " << endl; 
		delete orgVec[i]; 
    } 
    cout << "CrazyMonkey's final life point: " << crazyMonkey->getLifePoint() << endl ; 
 
    // Clean up 
    delete crazyMonkey; 
    delete [] orgVec; 
 
    return 0;
} 
