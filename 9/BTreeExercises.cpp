#include <iostream>
using std::cout;
using std::endl;
#include <cmath>
using std::pow;

#ifndef NULL
#define NULL 0
#endif

#ifndef MIN
#define MIN 0
#endif

#ifndef MAX
#define MAX 2147483647
#endif

#include "BTreeExercises.h"


// Basic:
void cleanUp(Node *root)
{
	if(root)
	{
		cleanUp(root->left);
		cleanUp(root->right);
	}
}

int treeHeight(Node *root)
{
	if(root == NULL)
		return 0;
	int lHeight = treeHeight(root->left);
	int rHeight = treeHeight(root->right);
	return 1 + ( lHeight > rHeight ? lHeight : rHeight);
}

int countNodes(Node *root)
{
	if(root == NULL)
		return 0;
	return 1 + countNodes(root->left) + countNodes(root->right);
}

void printInOrder(Node *root)
{
	if(root)
	{
		printInOrder(root->left);
		cout << root->data << " ";
		printInOrder(root->right);
	}
}

void printPostOrder(Node *root)
{
	if(root)
	{
		printPostOrder(root->left);
		printPostOrder(root->right);
		cout << root->data << " ";
	}
}

void printPreOrder(Node *root)
{
	if(root)
	{
		cout << root->data << " ";
		printPreOrder(root->left);
		printPreOrder(root->right);
	}
}


// Medium to Advance
/*
bool isPerfect(Node *);
bool isComplete(Node *root)
{
	if(root == NULL)
		return true;
	int lHeight = treeHeight(root->left);
	int rHeight = treeHeight(root->right);
	if(lHeight == rHeight)
		return isPerfect(root->left) && isComplete(root->right);
	if(lHeight - rHeight == 1)
		return isComplete(root->left) && isPerfect(root->right);
	return false;
}
*/
bool isComplete(Node *root, unsigned int index, unsigned int numOfNodes)
{
	if(root == NULL)
		return true;
	if(index >= numOfNodes)
		return false;
	return (isComplete(root->left, 2*index + 1, numOfNodes) && isComplete(root->right, 2*index + 2, numOfNodes));
}

bool isComplete(Node *root)
{
	return isComplete(root, 0, countNodes(root));
}

/* Method 1:
bool isBST(Node *root, int min, int max)
{
	if(root == NULL)
		return true;
	if(root->data < min || root->data > max)
		return false;
	return isBST(root->left, min, root->data-1) && isBST(root->right, root->data+1, max);
}

bool isBST(Node *root)
{
	return isBST(root, MIN, MAX);
}
*/

// Method 2
bool isBST(Node *root, Node *&prev)
{
	if(root)
	{
		if(!isBST(root->left, prev))
			return false;

		if(prev != NULL && root->data <= prev->data)
			return false;

		prev = root;

		return isBST(root->right, prev);
	}
	return true;
}

bool isBST(Node *root)
{
	Node *p = NULL;
	return isBST(root, p);
}

/* Method 3
int findMax(Node *);
int findMin(Node *);
bool isBST(Node *root)
{
	if(root == NULL)
		return true;
	if(root->left != NULL &&  root->data < findMax(root->left))
		return false;
	if(root->right != NULL && findMin(root->right) < root->data)
		return false;
	if(!isBST(root->left) || !isBST(root->right))
		return false;
	return true;
}
*/

Node *mirror(Node *root)
{
	if(root == NULL)
		return NULL;
	Node *mirror_tree = new Node(root->data);
	mirror_tree->left = mirror(root->right);
	mirror_tree->right = mirror(root->left);
	return mirror_tree;
}

// helper functions
bool isFull(Node *root)
{
	if(root == NULL)
		return true;
	if(root->left == NULL && root->right != NULL)
		return false;
	if(root->right == NULL && root->left != NULL)
		return false;
	return isFull(root->left) && isFull(root->right);
}

bool isPerfect(Node *root)
{
	if(root == NULL)
		return true;
	int num = countNodes(root);
	int height = treeHeight(root);
	if(num != pow(2, height) - 1)
		return false;
	return true;
}

// the binary search tree must be non-empty
int findMin(Node *root)
{
	Node *current = root;
	while(current->left)
		current = current->left;
	return current->data;
}

int findMax(Node *root)
{
	Node *current = root;
	while(current->right)
		current = current->right;
	return current->data;
}
