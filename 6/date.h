/**************************************/
// COMP2012 2015S Lab06
// Operator Overloading: Date
// Header file of the Date class: date.h
/***************************************/

#ifndef DATE_H
#define DATE_H

const int num_of_days_2015 = 365;
const int days_in_month[] = {
    31, // Jan 2015
    28, // Feb 2015
    31,
    30,
    31,
    30,
    31,
    31,
    30,
    31,
    30,
    31}; // Dec 2015

class Date // For the year 2015
{
  private:
    int days;

    /* Check if the input days d is out-of-bounds [1, 365]. If so, print an error
     * message and return false, else return true
     */
    bool assert_days(int d) const;
    
  public:
    Date(int n); // Set days; if out-of-bounds [1, 365], set days = 1
    int get_month() const; 
    int get_day() const; 

    /* For all operator functions, check if the result will be out of the bounds
     * [1, 365]. If not, proceed as usual; if yes, don't perform the action and
     * the Date object will not be modified 
     */
    // TODO: Add the operators to decrement the date: -, -=, -- (pre-decrement and post-dercrement operators)
    // TODO: Add the comparison operators to compare with another Date object: <, >, ==
    const Date& operator=(const Date&);
    const Date& operator+=(const Date&);
	const Date& operator-=(const Date&);
    Date operator+(const Date&) const;
	Date operator-(const Date&) const;
    Date& operator++(); // pre-increment
    Date& operator--(); // pre-decrement
    Date operator++(int); // post-increment
    Date operator--(int); // post-decrement
	bool operator<(const Date&) const;
	bool operator>(const Date&) const;
	bool operator==(const Date&) const;
};

#endif
