#include <iostream>
#include <fstream>
using namespace std;
#include "date.h"

// TODO: Add your implementation of the output operator, operator<<()
//       output the Date to the ostream, os, in format: 2015/MM/DD
ostream& operator<<(ostream &os, const Date &d)
{
	os << 2015 << "/" << d.get_month() << "/" << d.get_day();
	return os;
}

int main()
{
    ofstream myfile;
    myfile.open("output.txt");

    Date x(100);
    Date y(200);

    myfile << "x = " << x << endl
		   << "y = " << y << endl;;

    Date z = x + y;
    myfile << "z = x + y" << endl;
    myfile << "z = " << z << endl;
    myfile << "++z = " << ++z << endl;

    x += y;
    myfile << "x += y" << endl;
    myfile << "x = " << x << endl;;
    myfile << "x++ = " << x++ << endl;
    myfile << "x = " << x << endl;;

    if (x == y)
        myfile << "x is equal to y" << endl;
    if (x < y)
        myfile << "x is earlier than y" << endl;
    if (x > y)
        myfile << "x is later than y" << endl;

    z = x - y;
    myfile << "z = x - y" << endl;
    myfile << "z = " << z << endl;
    myfile << "-z = " << --z << endl;

    x -= y;
    myfile << "x -= y" << endl;
    myfile << "x = " << x << endl;;
    myfile << "x-- = " << x-- << endl;
    myfile << "x = " << x << endl;;

    if (x == y)
        myfile << "x is equal to y" << endl;
    if (x < y)
        myfile << "x is earlier than y" << endl;
    if (x > y)
        myfile << "x is later than y" << endl;

    z = x;
    myfile << "z = " << z << endl;
    if (z == x)
        myfile << "z is equal to x" << endl;
    else 
        myfile << "z is not equal to x" << endl;

    myfile.close();

    return 0;
}
