/**************************************/
// COMP2012 2015S Lab06
// Operator Overloading: Date
// Source file of the Date class: date.cpp
/***************************************/

#include <iostream>
using namespace std;
#include "date.h"

/* Check if the input days d is out-of-bounds [1, 365]. If so, print an error
 * message and return false, else return true
 */
bool Date:: assert_days(int d) const
{
    if (d < 1 || d > num_of_days_2015)
	{
		cerr << "Error: input days = " << d << " is invalid" << endl;
		return false;
	}
    else
        return true;
}

/* Constructor: to set days; if out-of-bounds [1, 365], set days = 1 */
Date::Date(int n)
{
    days = assert_days(n) ? n : 1;
}

/* Return the corresponding month (1 to 12) of the date */                
int Date::get_month() const
{
    int num_remaining_days = days;
    
    for (int m = 0; m < 12; ++m)
        if (num_remaining_days <= days_in_month[m])
            return m+1;
        else
            num_remaining_days -= days_in_month[m];

    return -1; // Shouldn't reach this line of code
}

/* Return the corresponding day of the date 
 * The number of days in each month is specified in the constant array, days_in_month */                
int Date::get_day() const
{
    int num_remaining_days = days;
    
    for (int m = 0; m < 12; ++m)
        if (num_remaining_days <= days_in_month[m])
            return num_remaining_days;
        else
            num_remaining_days -= days_in_month[m];

    return -1; // Shouldn't reach this line of code
}

// TODO: Add your implementation of the member operators
const Date& Date::operator=(const Date &d)
{
    days = d.days;
    return *this;
}

const Date& Date::operator+=(const Date &d)
{
    int day = days + d.days;
    if(assert_days(day))
	{
		days = day;
	}
    return *this;
}

const Date& Date::operator-=(const Date &d)
{
    int day = days - d.days;
    if(assert_days(day))
	{
		days = day;
	}
    return *this;
}

Date Date::operator+(const Date &d) const
{
    return Date(days + d.days);
}    

Date Date::operator-(const Date &d) const
{
    return Date(days - d.days);
}

Date& Date::operator++() // pre-increment
{
    if(assert_days(days + 1))
        ++days;
    return *this;
}

Date& Date::operator--() // pre-decrement
{
    if(assert_days(days - 1))
        --days;
    return *this;
}

Date Date::operator++(int) // post-increment
{
    if(assert_days(days + 1))
        return Date(days++);
    else
        return Date(1);
}


Date Date::operator--(int) // post-decrement
{
    if(assert_days(days - 1))
        return Date(days--);
    else
        return Date(1);
    
}

// TODO: Add the comparison operators to compare with another Date object: <, >, ==
bool Date::operator<(const Date &d) const
{
    return days < d.days;
}

bool Date::operator>(const Date &d) const
{
    return days > d.days;
}

bool Date::operator==(const Date &d) const
{
    return days == d.days;
}
