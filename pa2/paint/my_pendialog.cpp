/****************************************/
//  COMP2012 2015S PA2 -- Paint
//  File: my_pendialog.cpp
//  Description: the source file
//  -- MyPenDialog class implementation 
//     customizing the QDialog class for the Pen tool
/****************************************/

#include <iostream>
using std::cout;
using std::endl;

#include "my_pendialog.h"

MyPenDialog::MyPenDialog(QWidget* parent, const char* name)
 :QDialog(parent, name)
{
	// For setting the line's captstyle
	// Create a button group to contain all buttons
	capstylebgroup = new QHButtonGroup( "Cap Style", this );
	capstylebgroup->setGeometry( 0, 0, 300, 60 );
	connect( capstylebgroup, SIGNAL(clicked(int)), SLOT(OnSetCapStyle(int)) );

	QRadioButton *rb_flatcap = new QRadioButton( "Flat", capstylebgroup );
	QRadioButton *rb_squarecap = new QRadioButton( "Square", capstylebgroup );
	QRadioButton *rb_roundcap = new QRadioButton( "Round", capstylebgroup );
	rb_flatcap->setChecked( TRUE );
	
	pen.setCapStyle( Qt::FlatCap );
	pen.setJoinStyle( Qt::BevelJoin );

	// For setting the line width
	widthslider = new QSlider( QSlider::Horizontal, this, "width slider" );
	widthslider->setRange ( 1, 40 );
	widthslider->setLineStep ( 1 );
	widthslider->setValue ( pen.width() );
	widthslider->setGeometry  ( 80, 80, 190, 20 );
	widthslider->setTickmarks( QSlider::Below );
	widthslider->setTickInterval( 1 );

	connect( widthslider, SIGNAL(valueChanged(int)),
			 this, SLOT(OnSetPenWidth(int)) );

	wl = new QLabel( this );
	wl->setGeometry( 10,80,70,20 );
	wl->setText( "Pen Width" );

	widthlabel = new QLabel( this );
	widthlabel->setGeometry( 280, 80, 20, 20 );
	widthlabel->setNum( widthslider->value() );

	setFixedSize(300,110);
}

MyPenDialog::~MyPenDialog()
{
	delete capstylebgroup;

	delete widthslider;
	delete widthlabel;
	delete wl;
}

////////////////////////////////////////////////
// Handle Callback function
////////////////////////////////////////////////
// slot to set the pen width
void MyPenDialog::OnSetPenWidth(int w)
{
	widthlabel->setNum( w );
	pen.setWidth( w );
}

// slot to set the cap style
void MyPenDialog::OnSetCapStyle(int type)
{
	switch( type ){
	case 0:
		pen.setCapStyle( Qt::FlatCap );
		pen.setJoinStyle( Qt::BevelJoin );
		break;
	case 1:
		pen.setCapStyle( Qt::SquareCap );
		pen.setJoinStyle( Qt::MiterJoin );
		break;
	case 2:
		pen.setCapStyle( Qt::RoundCap );
		pen.setJoinStyle( Qt::RoundJoin );
		break;
	}
}
