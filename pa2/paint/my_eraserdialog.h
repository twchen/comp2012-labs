#include <qpen.h>
#include <qslider.h> 
#include <qdialog.h>
#include <qlabel.h>
#include <qlayout.h>

#ifndef _MY_ERASER_DIALOG_H
#define _MY_ERASER_DIALOG_H

class MyEraserDialog: public QDialog
{
	Q_OBJECT

public:
	MyEraserDialog(QWidget *parent = 0, const char *name = 0);
	~MyEraserDialog();

	QPen pen;
	QBrush brush;
	int width;

public slots:
	void OnSetEraserSize(int s);

private:
	QSlider *slider;
	QLabel *label;
	QLabel *value;
};

#endif
