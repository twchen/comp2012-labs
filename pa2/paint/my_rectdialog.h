#include <qpen.h>
#include <qslider.h> 
#include <qdialog.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qhbuttongroup.h>
#include <qvbuttongroup.h>
#include <qbuttongroup.h>
#include <qradiobutton.h>
#include <qbrush.h>
#include <qpainter.h>

#ifndef _DRAWTYPE
#define _DRAWTYPE
enum DrawType {
	Rect = 0,
	RoundRect,
	Circle,
	Ellipse
};
#endif

#ifndef _FILLCOLOR
#define _FILLCOLOR
enum FillColor {
	ForeGround = 0,
	BackGround
};
#endif

#ifndef _MY_RECT_DIALOG_H
#define _MY_RECT_DIALOG_H

class MyRectDialog: public QDialog
{
	Q_OBJECT

public:
	MyRectDialog(QWidget *parent = 0, const char *name = 0);
	~MyRectDialog();

	QPen pen;
	QBrush brush;

	DrawType drawType;
	FillColor fillColor;

public slots:
	void OnSetDrawType(int t);
	void OnSetFillStyle(int s);
	void OnSetBoundaryStyle(int s);
	void OnSetJoinStyle(int s);
	void OnSetFillColor(int c);
	void OnSetBoundaryWidth(int w);

private:
	// Drwa type button group
	QHButtonGroup *drawtypegroup;
	// fill style button group
	QButtonGroup *fillstylegroup;
	// boundary style button group
	QButtonGroup *boundarystylegroup;
	// boundary join style group
	QHButtonGroup *joinstylegroup;
	// fill color button group;
	QVButtonGroup *fillcolorgroup;

	QLabel *boundarywidth;
	QSlider *widthslider;
	QLabel *sldvalue;

};

#endif
