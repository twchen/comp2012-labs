#include <qpen.h>
#include <qslider.h>
#include <qdialog.h>
#include <qlabel.h>
#include <qvbuttongroup.h>
#include <qradiobutton.h>

#ifndef _LINETYPE
#define _LINETYPE

enum LineType {
	Single,
	Poly
};
#endif

#ifndef _MY_LINE_DIALOG_H
#define _MY_LINE_DIALOG_H

class MyLineDialog: public QDialog
{
	Q_OBJECT

public:
	MyLineDialog(QWidget *parent = NULL, const char *name = NULL);
	~MyLineDialog();

	//for storing the pen settings
	QPen pen;
	
	
	LineType linetype;
	bool polyBegined;

public slots:
	void OnSetLineStyle(int type);
	void OnSetLineType(int type);
	void OnSetCapStyle(int type);
	void OnSetLineWidth(int w);

private:
	
	// Line style button pointers;
	QVButtonGroup *linestylegroup;

	// Cap style button pointers;
	QVButtonGroup *capstylegroup;

	// Line type button pointers;
	QVButtonGroup *linetypegroup;

	
	// Slider pointer
	QSlider *widthslider;
	// Slider labels
	QLabel *widthlabel;
	QLabel *wl;
};
	
#endif
