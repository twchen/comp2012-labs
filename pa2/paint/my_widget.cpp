/****************************************/
//  COMP2012 2015S PA2 -- Paint
//  File: my_widget.cpp
//  Description: the source file
//  -- MyMainWindow class implementation 
/****************************************/

#include <iostream>
#include <cstdlib>
#include <algorithm>
using std::min;

#include "my_widget.h"

#include <qfiledialog.h>
#include <qpainter.h>
#include <qmessagebox.h>
#include <qinputdialog.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qpopupmenu.h>
#include <qtextedit.h>
#include <qfile.h>
#include <qfiledialog.h>
#include <qstatusbar.h>
#include <qmessagebox.h>
#include <qprinter.h>
#include <qapplication.h>
#include <qaccel.h>
#include <qtextstream.h>
#include <qpainter.h>
#include <qpaintdevicemetrics.h>
#include <qwhatsthis.h>
#include <qsimplerichtext.h>
#include <qcolordialog.h>
#include <qcolor.h> 
#include <qpointarray.h>

////////////////////////////////////////////////
// Constructor and Destructor
////////////////////////////////////////////////

MyMainWindow::MyMainWindow(QWidget* parent, const char* name)
	:QMainWindow(parent, name), undo(NULL), redo(NULL), image(new QPixmap()),
	 selectedbutton(DPen), mouseevent(0),
	 bgcolor(QColor(255, 255, 255)), paint(new QPainter(this))
{
	// build the menu
	CreateMenu();

	// build the tool bar 
	drawTools = new MyToolBar( this, "tool bar" );
	drawTools->setLabel( "Tool Bar" );

	connect( drawTools, SIGNAL( OnPaint() ), 
			 this, SLOT( OnDockWindowPositionChanged() ) );
   
	QPixmap pix;

	pix.load( "new_icon.bmp" );
	newButton = new QToolButton( pix, "New Image", QString::null,
								 this, SLOT( OnNewImage() ), drawTools, "new image" );

	pix.load( "open_icon.bmp" );
	openButton = new QToolButton( pix, "Open Image", QString::null,
								  this, SLOT( OnLoadImage() ), drawTools, "open image" );

	pix.load( "save_icon.bmp" );
	saveButton = new QToolButton( pix, "Save Image", QString::null,
								  this, SLOT( OnSaveImage() ), drawTools, "save image" );

	pix.load( "undo_icon.bmp" );
	undoButton = new QToolButton( pix, "Undo", QString::null,
								  this, SLOT( OnEditUndo() ), drawTools, "undo" );

	pix.load( "redo_icon.bmp" );
	redoButton = new QToolButton( pix, "Redo", QString::null,
								  this, SLOT( OnEditRedo() ), drawTools, "redo" );

	pix.load( "clearall_icon.bmp" );
	clearallButton = new QToolButton( pix, "Clear All", QString::null,
									  this, SLOT( OnEditClearAll() ), drawTools, "clear all" );

	pix.load( "resize_icon.bmp" );
	resizeButton = new QToolButton( pix, "Resize", QString::null,
									this, SLOT( OnEditResize() ), drawTools, "resize" );

	pix.load( "pen_icon.bmp" );
	penButton = new QToolButton( pix, "Pen", QString::null,
								 this, SLOT( OnChoosePen() ), drawTools, "pen" );
	pix.load( "line_icon.bmp" );
	lineButton = new QToolButton( pix, "Line", QString::null,
								  this, SLOT( OnChooseLine() ), drawTools, "line" );

	pix.load( "eraser_icon.bmp" );
	eraserButton = new QToolButton( pix, "Eraser", QString::null,
									this, SLOT( OnChooseEraser() ), drawTools, "eraser" );
   
	pix.load( "rect_icon.bmp" );
	rectButton = new QToolButton( pix, "Rectangle", QString::null,
								  this, SLOT( OnChooseRect() ), drawTools, "rectangle" );

	pix.load( "fcolor_icon.bmp" );
	colorButton = new QToolButton( pix, "Front Color", QString::null,
								   this, SLOT( OnChooseColor() ), drawTools, "front color" );

	pix.load( "bcolor_icon.bmp" );
	bcolorButton = new QToolButton( pix, "Backgound color", QString::null,
									this, SLOT( OnChooseBGColor() ), drawTools, "background color" );



	// create the pen dialog
	pendialog = new MyPenDialog( this, "Pen Dialog" );
	pendialog->setCaption( "Pen Dialog" );

	// create the line dialog
	linedialog = new MyLineDialog(this, "Line Dialog");
	linedialog->setCaption("Line Dialog");

	// create the eraser dialog
	eraserdialog = new MyEraserDialog(  this, "Eraser Dialog" );
	eraserdialog->setCaption("Eraser Dialog");

	// create the rectangle dialog
	rectdialog = new MyRectDialog(this, "Rectangle Dialog");
	rectdialog->setCaption("Rectangle Dialog");


	resize(800,600);

	QDockArea* leftDockWin = leftDock(); // return the left dock area;
	QDockArea* topDockWin = topDock();
      
	xPos = ((QWidget*)leftDockWin)->frameSize().width();
	yPos = menuBar()->height() + ((QWidget*)topDockWin)->frameSize().height();

}

MyMainWindow::~MyMainWindow()
{
	delete newButton;
	delete openButton;
	delete saveButton;
	delete undoButton;
	delete redoButton;
	delete clearallButton;
	delete resizeButton;
	delete rectButton;
	delete penButton;
	delete lineButton;
	delete eraserButton;
	delete colorButton;
	delete bcolorButton;
	delete drawTools;

	delete image;
	if(undo != NULL) delete undo;
	if(redo != NULL) delete redo;
	delete paint;

	delete pendialog;
	delete linedialog;
	delete rectdialog;
	delete eraserdialog;
   
	delete file;
	delete edit;
	delete view;
	delete help;
}

////////////////////////////////////////////////
// Create the menu
////////////////////////////////////////////////

void MyMainWindow::CreateMenu()
{
	// create the "File" popup menu
	file = new QPopupMenu( this );

	file->insertItem( "New image", this, SLOT( OnNewImage() ) );
	file->insertItem( "Load image", this, SLOT( OnLoadImage() ) );
	file->insertItem( "Save image", this, SLOT( OnSaveImage() ) );
	file->insertSeparator();
	file->insertItem( "Foregound Color", this, SLOT( OnChooseColor() ) );
	file->insertItem( "Backgound Color", this, SLOT( OnChooseBGColor() ) );
	file->insertSeparator();
	file->insertItem( "Exit", this, SLOT( OnExit() ) );
   
	menuBar()->insertItem( "File", file );

	// create the "Edit" popup menu
	edit = new QPopupMenu( this );
	edit->insertItem( "Undo", this, SLOT( OnEditUndo() ) );
	edit->insertItem( "Redo", this, SLOT( OnEditRedo() ) );
	file->insertSeparator();
	edit->insertItem( "Clear All", this, SLOT( OnEditClearAll() ) );
	file->insertSeparator();
	edit->insertItem( "Resize", this, SLOT( OnEditResize() ) );
	menuBar()->insertItem( "Edit", edit );

	view = new QPopupMenu( this );
	view->insertItem( "Tool Bar", this, SLOT( OnViewToolBar() ) );
	menuBar()->insertItem( "View", view );

	help = new QPopupMenu( this );
	help->insertItem( "About", this, SLOT( OnAbout() ) );
	menuBar()->insertItem( "Help", help );
}

////////////////////////////////////////////////
// Paint the image onto the widget 
////////////////////////////////////////////////

void MyMainWindow::paintbmp() const
{
	// close active painter
	if(paint->isActive () )
		{
			paint->end();
		}

	paint->begin( this );
	if ( ! image->isNull() ) 
		{
			paint->drawPixmap( xPos, yPos, (*image) );
		}
	paint->end();
}

////////////////////////////////////////////////
// Handle Paint Event
////////////////////////////////////////////////

void MyMainWindow::paintEvent(QPaintEvent* e)
{
	paintbmp();
}

////////////////////////////////////////////////
// Handle Callback function
////////////////////////////////////////////////

// Create a new image
void MyMainWindow::OnNewImage()
{
	bool ok;

	// get the user input of width from the input diaplog
	int width = QInputDialog::getInteger( "New Image", "Enter the bitmap width:", 400, 0, 10000, 1,
										  &ok, this );

	if ( !ok || width <= 0 ) 
		return;

	// get the user input of height from the input diaplog
	int height = QInputDialog::getInteger( "New Image", "Enter the bitmap height:", 300, 0, 10000, 1,
										   &ok, this );

	if ( !ok || height <= 0 ) 
		return;
   
	if ( image->isNull() )
		{
			delete image;   
		}

	// create an image with the input width & height
	image = new QPixmap( width, height );
	// fill it with default background
	image->fill( bgcolor );


	this->erase();
	paintbmp();
}

// Load an image
void MyMainWindow::OnLoadImage()
{
	QString s = QFileDialog::getOpenFileName( "", "*.bmp", this, "", "Load image..." );
	if ( ! s.isNull() ) 
		{
			if( image->load( s ) ){
				this->erase();
				paintbmp();
			}
		}
}

// Save the image as a bitmap
void MyMainWindow::OnSaveImage()
{
	QString s = QFileDialog::getSaveFileName( "", "*.bmp", this, "", "Save image..." );
	if (! s.isNull() )
		{
			image->save( s, "BMP" );
		}
}

// Change the foreground color
void MyMainWindow::OnChooseColor()
{
	QColor windowcolor = QColorDialog::getColor( pendialog->pen.color(), this );

	// what about line color, rect color?
	if( windowcolor.isValid() ){
		pendialog->pen.setColor( windowcolor );
		linedialog->pen.setColor( windowcolor );
		rectdialog->pen.setColor( windowcolor );
	}

}

// Change the background color
void MyMainWindow::OnChooseBGColor()
{
	QColor windowcolor = QColorDialog::getColor( bgcolor, this );
	if( windowcolor.isValid() ){
		bgcolor = windowcolor;
		eraserdialog->brush.setColor(bgcolor);
		eraserdialog->pen.setColor(
								   QColor(255 - bgcolor.red(), 255 - bgcolor.green(), 255 - bgcolor.blue()));
	}
}


void MyMainWindow::Backup(){
	delete redo;
	redo = NULL;
	delete undo;
	undo = new QPixmap(*image);
}

int MyMainWindow::getSign(int i){
	return ( i < 0 ) ? -1 : 1;
}

// Undo the last action
void MyMainWindow::OnEditUndo()
{
	// TODO!
	if(undo != NULL){
		delete redo;
		redo = image;
		image = undo;
		undo = NULL;
		this->erase();
		paintbmp();
	}
}

// Redo the last action
void MyMainWindow::OnEditRedo()
{
	// TODO!
	if( redo != NULL ){
		delete undo;
		undo = image;
		image = redo;
		redo = NULL;
		this->erase();
		paintbmp();
	}
}

// Clear the drawing
void MyMainWindow::OnEditClearAll()
{
	// TODO!
	Backup();
	image->fill(bgcolor);
	this->erase();
	paintbmp();
}

// Resize the image
void MyMainWindow::OnEditResize()
{
	// TODO!

	if(image->isNull())
		return;

	bool ok;

	// get the user input of width from the input diaplog
	int width = QInputDialog::getInteger( "New Image", "Enter the bitmap width:", image->width(), 0, 10000, 1,
										  &ok, this );

	if ( !ok || width <= 0 ) 
		return;

	// get the user input of height from the input diaplog
	int height = QInputDialog::getInteger( "New Image", "Enter the bitmap height:", image->height(), 0, 10000, 1,
										   &ok, this );

	if ( !ok || height <= 0 )
		return;

	if( width == image->width() && height == image->height() )
		return;
	
	Backup();
	delete image;
	image = new QPixmap(width, height);
	image->fill(bgcolor);
	if(paint->isActive())
		paint->end();
	paint->begin(image);
	paint->drawPixmap(0, 0, *undo, 0, 0, min(width, undo->width()), min(height, undo->height()));
	paint->end();
	this->erase();
	paintbmp();
}

// Show the tool bar
void MyMainWindow::OnViewToolBar()
{
	drawTools->show();
}

// Close the application
void MyMainWindow::OnExit()
{
	// qApp is a global pointer points to
	// our application object (QApplication)
	qApp->quit(); // quit this application program
}

// About Paint
void MyMainWindow::OnAbout()
{
	QMessageBox::about( this, "About", "CS2012 Spring 2015 Paint" );
}

// Activate the Pen tool
void MyMainWindow::OnChoosePen()
{
	selectedbutton = DPen;
	mouseevent = 0;
}

// Activate the other drawing tools
// TODO: other OnChooseXX()
void MyMainWindow::OnChooseLine()
{
	selectedbutton = DLine;
	mouseevent = 0;
}

void MyMainWindow::OnChooseRect()
{
	selectedbutton = DRect;
	mouseevent = 0;
}

void MyMainWindow::OnChooseEraser()
{
	selectedbutton = DEraser;
	mouseevent = 0;
}

////////////////////////////////////////////////
// Handle Mouse Event
////////////////////////////////////////////////
// Mouse press event handler
void MyMainWindow::mousePressEvent(QMouseEvent* e)
{
	// User presses the left button -- start drawing
	if ( e->button() == Qt::LeftButton )
		{
			mouseevent = 1;

			// Create a new image if necessary
			if ( image->isNull() ) 
				{ 
					OnNewImage(); 
				} 
			else 
				{ 
					if( paint->isActive() )
						{
							paint->end();
						}

					Backup();
					switch( selectedbutton ){
					case DPen:  // drawing with the Pen tool
						// update the cursor positions
						px = e->x(); py = e->y();
						dx = e->x(); dy = e->y();
						break;
					case DLine: // drawing with the Line tool
						// TODO:
						// drawing single line
						if(linedialog->linetype == Single){
							dx = e->x();
							dy = e->y();
						}
						// drawing polyline
						// the first line
						else if(linedialog->polyBegined){
							paint->begin(this);
							paint->setClipRect ( xPos, yPos, image->width(), image->height() );
							paint->setPen(linedialog->pen);
							paint->drawLine(dx, dy, e->x(), e->y());
							paint->end();
						}
						else{
							linedialog->polyBegined = true;
							dx = e->x();
							dy = e->y();
						}
						break;

					case DEraser: // drawing with the Eraser tool
						// TODO
						paint->begin(image);
						paint->setClipRect ( 0, 0, image->width(), image->height() );
						paint->setBackgroundColor(bgcolor);
						paint->eraseRect(
										 e->x() - (eraserdialog->width / 2) - xPos, e->y() - (eraserdialog->width / 2) - yPos,
										 eraserdialog->width, eraserdialog->width);
						paint->end();
						paintbmp();
						// draw border;
						paint->begin(this);
						paint->setPen(eraserdialog->pen);
						paint->setBrush(eraserdialog->brush);
						paint->setClipRect ( xPos, yPos, image->width(), image->height() );
						paint->drawRect(
										e->x() - (eraserdialog->width / 2), e->y() - (eraserdialog->width / 2),
										eraserdialog->width, eraserdialog->width);
						paint->end();
						break;

					case DRect:  // drawing witth the Rectangle tool
						// TODO
						dx = e->x();
						dy = e->y();
						break;
					}
				}
		}
	// User presses the right button -- show the tools' dialogs 
	else if( e->button() == Qt::RightButton )
		{
			mouseevent = 2;
			switch( selectedbutton ){
			case DPen:
				pendialog->show();
				break;
			case DLine:
				linedialog->polyBegined = false;
				linedialog->show();
				break;
			case DEraser:
				eraserdialog->show();
				break;
			case DRect:
				rectdialog->show();
				break;
			}
		}
}

// Mouse move event handler
void MyMainWindow::mouseMoveEvent(QMouseEvent* e)
{
	// dragging by left button click
	if( mouseevent == 1 )
		{
			if ( image->isNull() ) 
				{ 
				}
			else 
				{ 

					// For any temporary pen and brush settings
					// close active painter
					if( paint->isActive() )
						{
							paint->end();
						}

					switch( selectedbutton ){
					case DPen:  // drawing with the Pen tool
						{
							QPointArray a;
							paint->begin( image );
							paint->setClipRect ( 0, 0, image->width(), image->height() );
							paint->setPen( pendialog->pen ); 
							a.setPoints( 3, dx - xPos, dy - yPos,
										 px - xPos, py - yPos,
										 e->x() - xPos, e->y() - yPos );
							paint->drawPolyline( a, 0, 3 );               
							dx = px; dy = py;
							px = e->x(); py = e->y();
							paint->end();
							paintbmp();
						}
						break;

					case DLine: 
						paintbmp();
						// TODO
						paint->begin(this);
						paint->setClipRect ( xPos, yPos, image->width(), image->height() );
						paint->setPen(linedialog->pen);
						paint->drawLine(dx, dy, e->x(), e->y());
						paint->end();

						break;

					case DEraser:  // drawing with the Eraser tool
						// TODO

						paint->begin(image);
						paint->setClipRect ( 0, 0, image->width(), image->height() );
						paint->setBackgroundColor(bgcolor);
						paint->eraseRect(
										 e->x() - (eraserdialog->width / 2) - xPos, e->y() - (eraserdialog->width / 2) - yPos,
										 eraserdialog->width, eraserdialog->width);
						paint->end();
						paintbmp();
						paint->begin(this);
						paint->setClipRect ( xPos, yPos, image->width(), image->height() );
						paint->setPen(eraserdialog->pen);
						paint->setBrush(eraserdialog->brush);
						paint->drawRect(
										e->x() - (eraserdialog->width / 2), e->y() - (eraserdialog->width / 2),
										eraserdialog->width, eraserdialog->width);
						paint->end();
						break;

					case DRect:  // drawing with the Rectangle tool
						// TODO
						paintbmp();

						if(rectdialog->fillColor == ForeGround){
							rectdialog->brush.setColor(pendialog->pen.color());
						}
						else{
							rectdialog->brush.setColor(bgcolor);
						}

						paint->begin(this);
						paint->setPen(rectdialog->pen);
						paint->setBrush(rectdialog->brush);
						paint->setClipRect(xPos, yPos, image->width(), image->height());

						int w = e->x() - dx;
						int h = e->y() - dy;
						switch(rectdialog->drawType){
						case Rect:
							paint->drawRect(dx, dy, w, h);
							break;
						case RoundRect:
							paint->drawRoundRect(dx, dy, w, h);
							break;
						case Circle:
							{
								int diameter = min(abs(w), abs(h));
								paint->drawEllipse(dx, dy,
												   getSign(w) * diameter, getSign(h) * diameter);
							}
							break;
						case Ellipse:
							paint->drawEllipse(dx, dy, w, h);
							break;

						}
						paint->end();
						break;
					}                      
				}
		}
}

// Mouse release event handler
void MyMainWindow::mouseReleaseEvent(QMouseEvent* e)
{
	if( e->button() == Qt::LeftButton )
		{
			if ( image->isNull() ) 
				{ 
					//OnLoadImage(); 
				}
			else 
				{ 
					// For any temporary pen and brush settings


					// close active painter
					if( paint->isActive () )
						{
							paint->end();
						}

					switch( selectedbutton ){
					case DPen:
						{
							QPointArray a;
							paint->begin( image ); 
							paint->setClipRect ( 0, 0, image->width(), image->height() );
							paint->setPen( pendialog->pen );
							a.setPoints( 3, dx - xPos, dy - yPos,
										 px - xPos, py - yPos,
										 e->x() - xPos, e->y() - yPos );
							paint->drawPolyline( a, 0, 3 );             
							dx = px; dy = py;
							px = e->x(); py = e->y();
							paint->end();
						}
						break;

					case DLine:  // drawing with the Line tool 
						// TODO 
						paint->begin(image);
						paint->setPen(linedialog->pen);
						paint->drawLine(dx-xPos, dy-yPos, e->x()-xPos, e->y()-yPos);
						paint->end();
						dx = e->x();
						dy = e->y();

						break;

					case DEraser:  // drawing with the Eraser tool
						// TODO
						paint->begin(image);
						paint->setClipRect ( 0, 0, image->width(), image->height() );
						//paint->setBrush(eraserdialog->brush);
						paint->setBackgroundColor(bgcolor);
						paint->eraseRect(
										 e->x() - (eraserdialog->width / 2) - xPos, e->y() - (eraserdialog->width / 2) - yPos,
										 eraserdialog->width, eraserdialog->width);
						paint->end();
						break;

					case DRect:  // drawing with the Rectangle tool
						// TODO

						if(rectdialog->fillColor == ForeGround){
							rectdialog->brush.setColor(pendialog->pen.color());
						}
						else{
							rectdialog->brush.setColor(bgcolor);
						}

						paint->begin(image);
						paint->setPen(rectdialog->pen);
						paint->setBrush(rectdialog->brush);
						paint->setClipRect(0, 0, image->width(), image->height());

						int w = e->x()-dx;
						int h = e->y()-dy;
						switch(rectdialog->drawType){
						case Rect:
							paint->drawRect(dx-xPos, dy-yPos, w, h);
							break;
						case RoundRect:
							paint->drawRoundRect(dx-xPos, dy-yPos, w, h);
							break;
						case Circle:
							{
								int diameter = min(abs(w), abs(h));
								paint->drawEllipse(dx-xPos, dy-yPos,
												   getSign(w) * diameter, getSign(h) * diameter);
							}
							break;
						case Ellipse:
							paint->drawEllipse(dx-xPos, dy-yPos, w, h);
							break;

						}
						paint->end();

						break;
					}
					paintbmp();
				}
		}
	mouseevent = 0; // reset the mouse event type
}

// Mouse double click event handler
void MyMainWindow::mouseDoubleClickEvent(QMouseEvent* e)
{
	mouseevent = 0;
	linedialog->polyBegined = false;
}

////////////////////////////////////////////////
// Other user defined slots 
////////////////////////////////////////////////

void MyMainWindow::OnDockWindowPositionChanged()
{
	QDockArea* leftDockWin = leftDock();
	QDockArea* topDockWin = topDock();
      
	xPos = ((QWidget*)leftDockWin)->frameSize().width();
	yPos = menuBar()->height() + ((QWidget*)topDockWin)->frameSize().height();

	this->erase();
	paintbmp();
}
