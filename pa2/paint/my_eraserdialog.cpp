#include "my_eraserdialog.h"
#include <iostream>

MyEraserDialog::MyEraserDialog(QWidget *parent, const char *name)
	:QDialog(parent, name), width(10)
{
	

	// initialize pen width
	pen.setWidth(1);
	pen.setCapStyle(Qt::FlatCap);

	// For setting the line width
	slider = new QSlider( QSlider::Horizontal, this, "size slider" );
	slider->setRange ( 10, 100 );
	slider->setLineStep ( 1 );
	slider->setValue ( width );

	connect( slider, SIGNAL(valueChanged(int)),
			 this, SLOT(OnSetEraserSize(int)) );

	label = new QLabel( this );
	label->setText( "Size" );

	value = new QLabel( this );
	value->setNum( slider->value() );

	QHBoxLayout *hlayout = new QHBoxLayout(this);
	hlayout->addWidget(label);
	hlayout->addWidget(slider);
	hlayout->addWidget(value);
	setFixedSize(300, 30);

}

MyEraserDialog::~MyEraserDialog()
{
	std::cout << "~MyEraserDialog()" << std::endl << std::flush;
	delete slider;
	delete label;
	delete value;
}

void MyEraserDialog::OnSetEraserSize(int s)
{
	value->setNum(s);
	width = s;
}
