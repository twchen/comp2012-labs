#include <iostream>
#include "my_linedialog.h"

MyLineDialog::MyLineDialog(QWidget *parent, const char *name)
	:QDialog(parent, name), polyBegined(false), linetype(Single)
{
	linestylegroup = new QVButtonGroup("Line Style", this);
	linestylegroup->setGeometry( 5, 5, 180, 180 );

	QRadioButton *rb_solidline = new QRadioButton( "Solid line", linestylegroup );
	QRadioButton *rb_dashline = new QRadioButton("Dash line", linestylegroup);
	QRadioButton *rb_dotline = new QRadioButton("Dot line", linestylegroup);
	QRadioButton *rb_dashdotline = new QRadioButton("Dash dot line", linestylegroup);
	QRadioButton *rb_dashdotdotline = new QRadioButton("Dash dot dot line", linestylegroup);
	// set default line style
	pen.setStyle(Qt::SolidLine);
	rb_solidline->setChecked(TRUE);
	QObject::connect( linestylegroup, SIGNAL(clicked(int)), SLOT(OnSetLineStyle(int)) );
	
	capstylegroup = new QVButtonGroup("Cap Style", this);
	capstylegroup->setGeometry(200, 10, 100, 100);

	QRadioButton *rb_flatcap = new QRadioButton( "Flat", capstylegroup );
	QRadioButton *rb_squarecap = new QRadioButton( "Square", capstylegroup );
	QRadioButton *rb_roundcap = new QRadioButton( "Round", capstylegroup );
	
	pen.setCapStyle(Qt::FlatCap);
	rb_flatcap->setChecked(TRUE);
	QObject::connect( capstylegroup, SIGNAL(clicked(int)), SLOT(OnSetCapStyle(int)) );
	
	linetypegroup = new QVButtonGroup("Line Type", this);
	linetypegroup->setGeometry( 200, 120, 100, 90);

	QRadioButton *rb_singleline = new QRadioButton("Single", linetypegroup);
	QRadioButton *rb_polyline = new QRadioButton("Poly", linetypegroup);

	rb_singleline->setChecked(TRUE);
	QObject::connect( linetypegroup, SIGNAL(clicked(int)), SLOT(OnSetLineType(int)) );

	// For setting the line width
	widthslider = new QSlider( QSlider::Horizontal, this, "width slider" );
	widthslider->setRange ( 1, 40 );
	widthslider->setLineStep ( 1 );
	widthslider->setValue ( pen.width() );
	widthslider->setGeometry  ( 80, 220, 190, 20 );

	connect( widthslider, SIGNAL(valueChanged(int)),
			 this, SLOT(OnSetLineWidth(int)) );

	wl = new QLabel( "Line Width",this );
	wl->setGeometry( 10,220,70,20 );

	widthlabel = new QLabel( this );
	widthlabel->setGeometry( 280, 220, 20, 20 );
	widthlabel->setNum( widthslider->value() );

	setFixedSize(305, 245);
};

MyLineDialog::~MyLineDialog()
{
	std::cout << "~MyLineDialog()" << std::endl << std::flush;
	delete linestylegroup;
	delete capstylegroup;
	delete linetypegroup;
	delete widthslider;
	delete widthlabel;
	delete wl;
	
}

void MyLineDialog::OnSetLineStyle(int type)
{
	switch( type ){
	case 0:
		pen.setStyle(Qt::SolidLine);
		break;
	case 1:
		pen.setStyle(Qt::DashLine);
		break;
	case 2:
		pen.setStyle(Qt::DotLine);
		break;
	case 3:
		pen.setStyle(Qt::DashDotLine);
		break;
	case 4:
		pen.setStyle(Qt::DashDotDotLine);
		break;
	}
}

void MyLineDialog::OnSetLineType(int type)
{
	linetype = (type == 0) ? Single : Poly;
}

void MyLineDialog::OnSetCapStyle(int type)
{
	switch( type ){
	case 0:
		pen.setCapStyle( Qt::FlatCap );
		pen.setJoinStyle( Qt::BevelJoin );
		break;
	case 1:
		pen.setCapStyle( Qt::SquareCap );
		pen.setJoinStyle( Qt::MiterJoin );
		break;
	case 2:
		pen.setCapStyle( Qt::RoundCap );
		pen.setJoinStyle( Qt::RoundJoin );
		break;
	}
}

void MyLineDialog::OnSetLineWidth(int w)
{
	widthlabel->setNum( w );
	pen.setWidth( w );
}
