#include "my_rectdialog.h"
#include <iostream>
using std::cout;
using std::endl;

MyRectDialog::MyRectDialog(QWidget *parent, const char *name)
	:QDialog(parent, name), drawType(Rect), fillColor(ForeGround)
{
	
	drawtypegroup = new QHButtonGroup("Draw Type", this);
	drawtypegroup->setGeometry(5, 5, 500, 50);

	QRadioButton *rectangle = new QRadioButton("Rectangle", drawtypegroup);
	QRadioButton *roundrectangle = new QRadioButton("Round Rectangle", drawtypegroup);
	QRadioButton *circle = new QRadioButton("Circle", drawtypegroup);
	QRadioButton *ellipse = new QRadioButton("Ellipse", drawtypegroup);
	rectangle->setChecked(true);
	QObject::connect( drawtypegroup, SIGNAL(clicked(int)), this, SLOT(OnSetDrawType(int)) );
	
	pen.setStyle(Qt::SolidLine);
	brush.setStyle(Qt::NoBrush);

	fillstylegroup = new QButtonGroup(5, Qt::Horizontal, "Fill Style", this);
	fillstylegroup->setGeometry(5, 65, 500, 100);

	QRadioButton *solid  = new QRadioButton("Solid", fillstylegroup);   
	QRadioButton *dense1 = new QRadioButton("Dense 1", fillstylegroup);
	QRadioButton *dense2 = new QRadioButton("Dense 2", fillstylegroup);
	QRadioButton *dense3 = new QRadioButton("Dense 3", fillstylegroup);
	QRadioButton *dense4 = new QRadioButton("Dense 4", fillstylegroup);
	QRadioButton *dense5 = new QRadioButton("Dense 5", fillstylegroup);
	QRadioButton *dense6 = new QRadioButton("Dense 6", fillstylegroup);
	QRadioButton *dense7 = new QRadioButton("Dense 7", fillstylegroup);
	QRadioButton *horizontal = new QRadioButton("Horizontal", fillstylegroup);
	QRadioButton *vertical = new QRadioButton("Vertical", fillstylegroup);
	QRadioButton *cross = new QRadioButton("Cross", fillstylegroup);
	QRadioButton *bdiag = new QRadioButton("Bdiag", fillstylegroup);
	QRadioButton *fdiag = new QRadioButton("Fdiag", fillstylegroup);
	QRadioButton *cdiag = new QRadioButton("Cdiag", fillstylegroup);
	QRadioButton *nopattern = new QRadioButton("No Pattern", fillstylegroup);

	nopattern->setChecked(true);
	QObject::connect( fillstylegroup, SIGNAL(clicked(int)), this, SLOT(OnSetFillStyle(int)) );

	boundarystylegroup = new QButtonGroup(3, Qt::Horizontal, "Boundary Style", this);
	boundarystylegroup->setGeometry(5, 175, 365, 75);

	QRadioButton *solidline = new QRadioButton("Solid Line", boundarystylegroup);
	QRadioButton *dashline = new QRadioButton("Dash Line", boundarystylegroup);
	QRadioButton *dotline = new QRadioButton("Dot Line", boundarystylegroup);
	QRadioButton *dashdotline = new QRadioButton("Dash Dot Line", boundarystylegroup);
	QRadioButton *dashdotdotline = new QRadioButton("Dash Dot Dot Line", boundarystylegroup);
	QRadioButton *noline = new QRadioButton("No Line", boundarystylegroup);

	solidline->setChecked(true);
	QObject::connect( boundarystylegroup, SIGNAL(clicked(int)), this, SLOT(OnSetBoundaryStyle(int)) );

	joinstylegroup = new QHButtonGroup("Boundary Join Style", this);
	joinstylegroup->setGeometry(5, 260, 365, 50);

	QRadioButton *miterjoin = new QRadioButton("Miter Join", joinstylegroup);
	QRadioButton *beveljoin = new QRadioButton("Bevel Join", joinstylegroup);
	QRadioButton *roundjoin = new QRadioButton("Round Join", joinstylegroup);
	miterjoin->setChecked(true);
	QObject::connect( joinstylegroup, SIGNAL(clicked(int)), this, SLOT(OnSetJoinStyle(int)) );

	fillcolorgroup = new QVButtonGroup("Fill Color", this);
	fillcolorgroup->setGeometry(380, 175, 120, 135);

	QRadioButton *foreground = new QRadioButton("Fore-ground", fillcolorgroup);
	QRadioButton *background = new QRadioButton("Back-ground", fillcolorgroup);
	foreground->setChecked(true);
	QObject::connect( fillcolorgroup, SIGNAL(clicked(int)), this, SLOT(OnSetFillColor(int)) );

	boundarywidth = new QLabel("Boundary Width", this);
	boundarywidth->setGeometry(5, 315, 120, 20);

	widthslider = new QSlider( QSlider::Horizontal, this, "width slider");
	widthslider->setGeometry(130, 315, 330, 20);
	widthslider->setRange(1,40);
	widthslider->setLineStep(1);

	widthslider->setValue(pen.width());

	QObject::connect(widthslider, SIGNAL(valueChanged(int)), this, SLOT(OnSetBoundaryWidth(int)) );

	sldvalue = new QLabel(this);
	sldvalue->setGeometry(465, 315, 35, 20);
	sldvalue->setNum(widthslider->value());
	setFixedSize(510, 340);
}

MyRectDialog::~MyRectDialog()
{
	std::cout << "~MyRectDialog()" << std::endl << std::flush;
	delete drawtypegroup;
	delete fillstylegroup;
	delete boundarystylegroup;
	delete joinstylegroup;
	delete fillcolorgroup;
	delete boundarywidth;
	delete widthslider;
	delete sldvalue;
}

void MyRectDialog::OnSetDrawType(int t)
{
	switch(t){
	case 0:
		drawType = Rect;
		break;
	case 1:
		drawType = RoundRect;
		break;
	case 2:
		drawType = Circle;
		break;
	case 3:
		drawType = Ellipse;
		break;
	}
}

void MyRectDialog::OnSetFillStyle(int s)
{
	switch(s){
	case 0:
		brush.setStyle(Qt::SolidPattern);
		break;
	case 1:
		brush.setStyle(Qt::Dense1Pattern);
		break;
	case 2:
		brush.setStyle(Qt::Dense2Pattern);
		break;
	case 3:
		brush.setStyle(Qt::Dense3Pattern);
		break;
	case 4:
		brush.setStyle(Qt::Dense4Pattern);
		break;
	case 5:
		brush.setStyle(Qt::Dense5Pattern);
		break;
	case 6:
		brush.setStyle(Qt::Dense6Pattern);
		break;
	case 7:
		brush.setStyle(Qt::Dense7Pattern);
		break;
	case 8:
		brush.setStyle(Qt::HorPattern);
		break;
	case 9:
		brush.setStyle(Qt::VerPattern);
		break;
	case 10:
		brush.setStyle(Qt::CrossPattern);
		break;
	case 11:
		brush.setStyle(Qt::BDiagPattern);
		break;
	case 12:
		brush.setStyle(Qt::FDiagPattern);
		break;
	case 13:
		brush.setStyle(Qt::DiagCrossPattern);
		break;
	case 14:
		brush.setStyle(Qt::NoBrush);
		break;
	}
}

void MyRectDialog::OnSetBoundaryStyle(int s)
{
	switch(s){
	case 0:
		pen.setStyle(Qt::SolidLine);
		break;
	case 1:
		pen.setStyle(Qt::DashLine);
		break;
	case 2:
		pen.setStyle(Qt::DotLine);
		break;
	case 3:
		pen.setStyle(Qt::DashDotLine);
		break;
	case 4:
		pen.setStyle(Qt::DashDotDotLine);
		break;
	case 5:
		pen.setStyle(Qt::NoPen);
		break;
	}
}
void MyRectDialog::OnSetJoinStyle(int s)
{
	switch(s){
	case 0:
		pen.setJoinStyle(Qt::MiterJoin);
		break;
	case 1:
		pen.setJoinStyle(Qt::BevelJoin);
		break;
	case 2:
		pen.setJoinStyle(Qt::RoundJoin);
		break;
	}
}
void MyRectDialog::OnSetFillColor(int c)
{
	fillColor = ( c ) ? BackGround : ForeGround;
}
void MyRectDialog::OnSetBoundaryWidth(int w)
{
	sldvalue->setNum(w);
	pen.setWidth(w);
}
