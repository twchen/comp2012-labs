#include <set>
#include <algorithm>
#include <string>
#include <iostream>
#include <vector>
using namespace std;

template <typename Container>
void printUsingIterator(const Container &c)
{
	cout << "{";
	typename Container::const_iterator it = c.begin();
	if(it != c.end()){
		cout << *it;
		++it;
	}
	for( ; it != c.end(); ++it)
		cout << "," << *it;
	cout << "}" << endl;
}

void printSetUsingIterator(const set<string>& s) {
	// Print a set of string using iterator
	printUsingIterator(s);
}

void printVectorUsingIterator(const vector<string>& s) {
	// Print a vector of string using iterator
	printUsingIterator(s);
}


int main() {

    set<string> aSet, bSet;

	aSet.insert("CSE Alumni Association");
	aSet.insert("CSE Entrepreneurship Club");
	aSet.insert("CSE One");

	bSet.insert("CSE Entrepreneurship Club");
	bSet.insert("CSE Mentorship Program");
	bSet.insert("CSE TechShare");
	bSet.insert("Hackathon@HKUST");

	cout << "Set A Content =" ;
	printSetUsingIterator(aSet);
	cout << "Set B Content =" ;
	printSetUsingIterator(bSet);
	
	// Part 1: Complete the set operations here..
	

	// set union
	vector<string> v1;
	set_union(aSet.begin(), aSet.end(), bSet.begin(), bSet.end(), back_inserter(v1));
	cout << "A union B Content = ";
	printVectorUsingIterator(v1);

	// set intersection
	vector<string> v2;
	set_intersection(aSet.begin(), aSet.end(), bSet.begin(),bSet.end(), back_inserter(v2));
	cout << "A intersect B Content = ";
	printVectorUsingIterator(v2);

	// set difference
	vector<string> v3;
	set_difference(aSet.begin(), aSet.end(), bSet.begin(), bSet.end(), back_inserter(v3));
	cout << "A difference B Content = ";
	printVectorUsingIterator(v3);




	// Part 2: Merge setA and setB to vectorR
	// Part 3: Complete the vector operations here..
	
	vector<string> vecR ;
	
	cout << "Vector R Content = ";
	vecR.insert(vecR.begin(), aSet.begin(), aSet.end());
	vecR.insert(--vecR.end(), bSet.begin(), bSet.end());
	printVectorUsingIterator(vecR);
	
	cout << "Sorted R Content = ";
	sort(vecR.begin(), vecR.end());
	printVectorUsingIterator(vecR);
	
	cout << "Unique R Content = ";
	vector<string>::iterator new_end = unique(vecR.begin(), vecR.end());
	vecR.resize(new_end - vecR.begin());
	printVectorUsingIterator(vecR);

	cout << "Reversed Content = ";
	reverse(vecR.begin(), vecR.end());
	printVectorUsingIterator(vecR);
	
	cout << "Reverse each string = ";
	for(vector<string>::iterator it = vecR.begin(); it != vecR.end(); ++it)
		reverse((*it).begin(), (*it).end());
	printVectorUsingIterator(vecR);
	
	return 0;
}
