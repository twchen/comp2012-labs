template <typename T>
set<T> setUnion(const set<T> &s1, const set<T> &s2)
{
	set<T> result(s1);
	result.insert(s2.begin(), s2.end());
	return result;
}

template <typename T>
set<T> setIntersect(const set<T> &s1, const set<T> &s2)
{
	set<T> result(s1);
	for(typename set<T>::const_iterator it = s1.begin(); it != s1.end(); ++it){
		if(s2.find(*it) == s2.end())
			result.erase(*it);
	}
	return result;
}

template <typename T>
set<T> setDifference(const set<T> &s1, const set<T> &s2)
{
	set<T> result(s1);
	for(typename set<T>::const_iterator it = s1.begin(); it != s1.end(); ++it){
		if(s2.find(*it) != s2.end())
			result.erase(*it);
	}	
	return result;
}

