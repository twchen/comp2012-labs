QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = paint
TEMPLATE = app

HEADERS += \
    mainwindow.h \
    painttool.h \
    linetool.h \
    erasertool.h \
    recttool.h \
    curvetool.h \
    canvas.h

SOURCES += \
    mainwindow.cpp \
    painttool.cpp \
    linetool.cpp \
    erasertool.cpp \
    recttool.cpp \
    curvetool.cpp \
    canvas.cpp \
    main.cpp
