#ifndef PAINTTOOL_H
#define PAINTTOOL_H

#include <QWidget>

class PaintTool : public QWidget
{
    Q_OBJECT
public:
    explicit PaintTool(QWidget *parent = 0);
    virtual ~PaintTool();

signals:

public slots:
};

#endif // PAINTTOOL_H
