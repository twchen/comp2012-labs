#ifndef CANVAS_H
#define CANVAS_H

#include <QWidget>
#include <QPixmap>
#include <list>
#include "painttool.h"
#include "curvetool.h"
#include "linetool.h"
#include "erasertool.h"
#include "recttool.h"

class Canvas : public QWidget
{
    Q_OBJECT
public:
    explicit Canvas(QWidget *parent = 0);
    ~Canvas();

signals:

public slots:

private:
    list<QPixmap> undo;
    list<QPixmap> redo;
    QPixmap *current;
    CurveTool *curve;
    LineTool *line;
    EraserTool *eraser;
    RectTool *rect;
    PaintTool *currentTool;

};

#endif // CANVAS_H
