#ifndef ERASERTOOL_H
#define ERASERTOOL_H

#include "painttool.h"

class EraserTool : public PaintTool
{
public:
    EraserTool();
    ~EraserTool();
};

#endif // ERASERTOOL_H
