#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenuBar>
#include <QToolBar>
#include "canvas.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:

public slots:


private:
    QMenuBar *menu;
    QToolBar *toolbar;
    Canvas *canvas;
};

#endif // MAINWINDOW_H
