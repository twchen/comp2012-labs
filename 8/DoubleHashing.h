#include "OpenAddressing.h"

class DoubleHashing: public OpenAddressing
{
public:
	DoubleHashing(int size): OpenAddressing(size) {}
	int hash2(int value);
	virtual int nextEmpty(int nonEmptyIndex, int value);
};
