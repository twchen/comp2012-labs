#include "QuadraticProbing.h"

int QuadraticProbing::nextEmpty(int nonEmptyIndex, int value)
{
	int i = 1;
	while(data[(nonEmptyIndex+i*i) % currSize] != EMPTY)
		++i;
	return (nonEmptyIndex + i*i) % currSize;
}
