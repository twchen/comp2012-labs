#ifndef LINEAR_PROBING_H
#define LINEAR_PROBING_H

#include "OpenAddressing.h"

class LinearProbing:public OpenAddressing
{
public:
	LinearProbing(int size): OpenAddressing(size) {}
	virtual int nextEmpty(int nonEmptyIndex, int value);
};

#endif
