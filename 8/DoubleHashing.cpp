#include "DoubleHashing.h"

int DoubleHashing::hash2(int value)
{
	return 7 - value % 7;
}

int DoubleHashing::nextEmpty(int nonEmptyIndex, int value)
{
	int i = 1;
	while(data[(nonEmptyIndex + i*hash2(value)) % currSize] != EMPTY)
		  ++i;
	return ( nonEmptyIndex + i*hash2(value) ) % currSize;
}
