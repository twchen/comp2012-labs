#include "LinearProbing.h"
/*
int LinearProbing::hash(int value)
{
	return value % currSize;
}
*/

int LinearProbing::nextEmpty(int nonEmptyIndex, int value)
{
	int i = 1;
	while(data[(nonEmptyIndex+i) % currSize] != EMPTY)
		++i;
	return (nonEmptyIndex + i)% currSize;
}
