#include "OpenAddressing.h"

class QuadraticProbing:public OpenAddressing
{
public:
	QuadraticProbing(int size): OpenAddressing(size) {}
	virtual int nextEmpty(int nonEmptyIndex, int value);
};

