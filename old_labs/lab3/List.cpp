#include "List.h"

void deleteList(NodePtr& m_Head)
{
	NodePtr ptrTemp;
	while (m_Head != NULL)
	{
		ptrTemp = m_Head;
		m_Head = m_Head->ptrNext;
		delete ptrTemp;
	}
}


void Print(NodePtr m_Head)
{
	NodePtr ptrTemp = m_Head;
	// add the code to print the node str 
	//
	if(m_Head == NULL){
		cout << "NIL" << endl;
		return;
	}
	while(ptrTemp != NULL){
		cout << ptrTemp->str;
		if(ptrTemp->ptrNext != NULL)
			cout << "->";
		ptrTemp = ptrTemp->ptrNext;
	}
	cout << endl;
}

int FindDistance(NodePtr m_Head, string str1, string str2)
{
	if (m_Head == NULL)		// empty list
		return -1;
	else {
		if (str1 == str2) {
			return 0;
 		}
		else {
			// To add some code here
			NodePtr n1 = m_Head;
			while(n1 != NULL && n1->str != str1){
				n1 = n1->ptrNext;
			}
			if(n1 != NULL){
				NodePtr n2 = n1->ptrNext;
				int dist = 1;
				while(n2 != NULL && n2->str != str2){
					n2 = n2->ptrNext;
					++dist;
				}
				if(n2 != NULL)
					return dist;
			}
		}
		return -1;
	}
}

bool Insert(NodePtr& m_Head, string str)
{
	if (str == "q") {
		cerr << "invalid string\n";
		return false;
	}

	if (m_Head == NULL) {	// empty list
		m_Head = new Node;
		m_Head->str = str;
		m_Head->ptrNext = NULL;
	}
	else {
		//Create new node
		NodePtr ptrNew = new Node;
		ptrNew->str = str;
		ptrNew->ptrNext = NULL;

		NodePtr ptrTemp = m_Head;
		while (ptrTemp->ptrNext != NULL)
		{
			if (ptrTemp->str == str)	// discard duplicated node
				return false;
			ptrTemp = ptrTemp->ptrNext;
		}
		if (ptrTemp->str == str)	// checking the last node
			return false;

		//Append to last
		ptrTemp->ptrNext = ptrNew;
	}
	return true;
}
