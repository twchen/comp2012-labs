#include <string>
#include "stringset.h"
using namespace std;

// ============================
// Constructors and destructor
// ============================

// Default constructor: Create an empty set
StringSet::StringSet()
{
	size = 0;
	capacity = 0;
	arr = NULL;
}

// Destructor: Deallocate memory
StringSet::~StringSet()
{
	if(arr != NULL)
		delete [] arr;
}

// Constructor: Create a set with a single item
StringSet::StringSet(const string& oneItem)
{
	size = 1;
	capacity = 2;
	arr = new string[2];
	arr[0] = oneItem;
}

// Constructor: Create a set using an array of string
StringSet::StringSet(const string inputStrArr[], int sz)
{
	size = sz;
	capacity = sz;//==================== Q: or should be capacity = 2*sz ?
	arr = new string[capacity];
	for(int i=0; i<sz; ++i)
		arr[i] = inputStrArr[i];
}

// Copy constructor
StringSet::StringSet(const StringSet& otherSet)
{
	size = otherSet.size;
	capacity = otherSet.capacity;
	arr = new string[capacity];
	for(int i=0; i<size; ++i)
		arr[i] = otherSet.arr[i];
}


// =====================
// Member functions
// ====================

// Return a string located at the given array index
// Note: No need to do any boundary checking in this function
string StringSet::getItem(int index) const
{
	return arr[index];
}

// Return size
int StringSet::getSize() const
{
	return size;
}

// Return capacity
int StringSet::getCapacity() const
{
	return capacity;
}

// Return true if a set is an empty set. Otherwise, return false
bool StringSet::isEmpty() const
{
	return size == 0;
}

// Return true if an item exists in a set. Otherwise, return false
bool StringSet::exists(const string& item) const
{
	for(int i=0; i<size; ++i)
		if(arr[i] == item)
			return true;
	return false;
}


// Return the index of an item
// Return -1 if the item can't be found
int StringSet::searchItem(const string& item) const
{
	for(int i=0; i<size; ++i)
		if(arr[i] == item)
			return i;
	return -1;
}


// If newItem exists, don't insert the newItem and return false
// Otherwise, insert newItem and then return true
bool StringSet::insert(const string& newItem)
{
	if(exists(newItem))
		return false;
	
	if(size == 0){
		//*this = StringSet(newItem); // wrong, because no assignment operator is defined. But can be ' assign(StringSet(newItem)); '
		size = 1;
		capacity = 2;
		arr = new string[2];
		arr[0] = newItem;
	}
	else if(size + 1 > capacity){
		capacity = 2 * (size + 1);//============== bug4: misunderstand doubling capacity. ( capacity *= 2 )
		string* temp = arr;
		arr = new string[capacity];
		for(int i=0; i<size; ++i)
			arr[i] = temp[i];
		arr[size] = newItem;
		++size;//=============================== bug1: forget to increase the size by 1
		delete [] temp;
	}
	else{
		arr[size] = newItem;
		++size;
	}
	return true;
}


// If an item exists, remove that item from the set and return true
// Otherwise, return false
bool StringSet::remove(const string& item)
{
	int index = searchItem(item);
	if(index == -1){
		return false;
	}
	
	if(size == 1){
		deallocateArray();
		return true; //============================ bug2: does not terminate after deallocation, so size = 0 and then --size
	}

	if(size - 1 < capacity / 2){// half capacity
		capacity /= 2;
		string *temp = arr;
		arr = new string[capacity];
		for(int i=0, j=0; i<size; ++i){
			if(i == index)
				continue;
			arr[j] = temp[i];
			++j;
		}
		--size;
		delete [] temp;//=========================== bug5: forget to delete the old array
		return true;
	}

	if(index != size-1){
		arr[index] = arr[size-1]; // the last string will be used to overwrite the string pending to be removed
	}
	--size;
	return true;
		
}


// Sort the items
// void sort()  {}


// Note: You should implement "Deep copy" as follows:
//    (1) Deallocate the content of the current array
//    (2) Allocate a new array with the same size and capacity as inputSet
//    (3) Copy all items from inputSet
//    (4) Return the reference of the current object (i.e. return *this)
StringSet& StringSet::assign(const StringSet& inputSet)
{
	delete [] arr;
	size = inputSet.size;
	capacity = inputSet.capacity;
	arr = new string[capacity];
	for(int i=0; i<size; ++i)
		arr[i] = inputSet.arr[i];
	return *this;
}


// Check whether the current set is equal to another set
// Note: The order is NOT important
//    e.g. {"Hong Kong", "London"} should be the same as {"London", "Hong Kong"}
bool StringSet::equals(const StringSet& other) const
{
	if(size != other.size)
		return false;
	
	for(int i=0; i<size; ++i){
		if(!exists(other.arr[i]))
			return false;
	}
	return true;
}


// Check whether the current set is not equal to another set
bool StringSet::notEquals(const StringSet& other) const
{
	if(size != other.size)
		return true;

	for(int i=0; i<size; ++i){
		if(!exists(other.arr[i]))
			return true;
	}
	return false;
}


//  Implement the set union operation
StringSet& StringSet::setUnion(const StringSet& other)
{
	
	for(int i=0; i<other.size; ++i){
		if(!exists(other.arr[i]))
			insert(other.arr[i]);
	}
	return *this;

	/*	
	StringSet set(*this);
	for(int i=0; i<other.size; ++i)
		if(!set.exists(other.arr[i]))
			set.insert(other.arr[i]);
	return set;
	*/
}


//  Implement the set intersect operation
StringSet& StringSet::setIntersect(const StringSet& other)// Be careful about returning reference, because a reference of local variable might be returned;
{
	/*
	for(int i=0; i<size; ++i)
		if(!other.exists(arr[i])){
			//cout << arr[i] << endl;//======================== debug
			remove(arr[i]);
		}
	return *this;
	*/
	//================================= bug3: after removing one item, the size and order changes, cannot traverse all items
	/*
	StringSet set(*this);
	for(int i=0; i<set.size; ++i)
		if(!other.exists(set.arr[i]))
			remove(set.arr[i]);
	return *this;
	*/
	
	// More efficient implementation
	int i = 0;
	while(i < size){
		if(!other.exists(arr[i])){
			remove(arr[i]);
			continue;
		}
		++i;
	}
	return *this;
}


//  Implement the set difference operation
StringSet& StringSet::setDifference(const StringSet& other)
{
	/*
	for(int i=0; i<size; ++i)
		if(other.exists(arr[i]))
			remove(arr[i]);
	return *this;
	*/
	
	//================================= bug3: see above
	/*
	StringSet set(*this);
	for(int i=0; i<set.size; ++i)
		if(other.exists(set.arr[i]))
			remove(set.arr[i]);
	return *this;
	*/

	// More efficient implementation
	int i=0;
	while(i < size){
		if(other.exists(arr[i])){
			remove(arr[i]);
			continue;
		}
		++i;
	}
	return *this;
}

// Private member function,
//    deallocate arr and reset the size and capacity to 0
void StringSet::deallocateArray()
{
	size = 0;
	capacity = 0;
	delete [] arr;
}


//   Global functions
//   Implement the set union
StringSet setUnion(const StringSet& first, const StringSet& second)
{
	StringSet s(first);
	return s.setUnion(second);
}


//   Implement the set intersect
StringSet setIntersect(const StringSet& first, const StringSet& second)
{
	StringSet s(first);
	return s.setIntersect(second);
}


//   Implement the set difference
StringSet setDifference(const StringSet& first, const StringSet& second)
{
	StringSet s(first);
	return s.setDifference(second);
}
