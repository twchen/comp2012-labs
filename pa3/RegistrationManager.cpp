/*
 * RegistrationManager.cpp
 *
 *  Created on: Jan 16, 2015
 *      Author: cspeter
 */


#include "RegistrationManager.h"

#include <string>
using namespace std;

// TODO: 
// ========================================================================
// Implementation of RegistrationManager
// ========================================================================

RegistrationManager::RegistrationManager(int numOfStudentBuckets, int numOfCourseBuckets)
	: m_numOfStudentBuckets(numOfStudentBuckets),
	  m_numOfCourseBuckets(numOfCourseBuckets),
	  m_studentTable(new HashTable<Student>(numOfStudentBuckets, Student::hash)),
	  m_courseTable(new HashTable<Course>(numOfCourseBuckets, Course::hash))
{

}

RegistrationManager::~RegistrationManager()
{
	delete m_studentTable;
	delete m_courseTable;
}

// Student Management
// return true if student not exist before
bool RegistrationManager::newStudent(const Student& stu)
{
	if(m_studentTable->isExist(stu))
		return false;
	m_studentTable->insert(stu);
	return true;
}
	
// return true if student exist before
bool RegistrationManager::modifyStudent(const Student& modifyStu)
{
	if(m_studentTable->isExist(modifyStu))
	{
		m_studentTable->deleteElem(modifyStu);
		m_studentTable->insert(modifyStu);
		return true;
	}
	return false;
}
	
// return true if studentID exist before
bool RegistrationManager::deleteStudent(const string& studentID)
{
	if(isStudentExist(studentID))
	{
		m_studentTable->deleteElem(Student(studentID));
		return true;
	}
	return false;
}
	
// return true if studentID exist before
bool RegistrationManager::isStudentExist(const string& studentID) const
{
	return m_studentTable->isExist(Student(studentID));
}

// return true if studentID exist before
bool RegistrationManager::retrieveStudent(const string& studentID, Student& student) const
{
	HashTable<Student>::ConstIterator itc = m_studentTable->search(Student(studentID));
	if(itc != m_studentTable->end())
	{
		student = *itc;
		return true;
	}
	return false;
}


// Course Management
// return true if course not exist before
bool RegistrationManager::newCourse(const Course& course)
{
	if(m_courseTable->isExist(course))
		return false;
	m_courseTable->insert(course);
	return true;
}

// return true if course exist before
bool RegistrationManager::modifyCourse(const Course& modifyCourse)
{
	if(m_courseTable->isExist(modifyCourse))
	{
		m_courseTable->deleteElem(modifyCourse);
		m_courseTable->insert(modifyCourse);
		return true;
	}
	return false;
}

// return true if courseCode exist before
bool RegistrationManager::deleteCourse(const string& courseCode)
{
	if(isCourseExist(courseCode))
	{
		m_courseTable->deleteElem(Course(courseCode));
		return true;
	}
	return false;
}

// return true if courseCode exist before
bool RegistrationManager::isCourseExist(const string& courseCode) const
{
	return m_courseTable->isExist(Course(courseCode));
}

// return true if courseCode exist before
bool RegistrationManager::retrieveCourse(const string& courseCode, Course& course) const
{
	HashTable<Course>::ConstIterator itc = m_courseTable->search(Course(courseCode));
	if(itc != m_courseTable->end())
	{
		course = *itc;
		return true;
	}
	return  false;
}

// Query Management
// output to studentList (sorted)
void RegistrationManager::getAllStudents(DoublyLinkedList<Student>& studentList) const
{
	studentList.clear();
	HashTable<Student>::ConstIterator itc = m_studentTable->begin();
	for(; itc != m_studentTable->end(); ++itc)
		studentList.insert(*itc);
}

// output to courseList (sorted)
void RegistrationManager::getAllCourses(DoublyLinkedList<Course>& courseList) const
{
	courseList.clear();
	HashTable<Course>::ConstIterator itc = m_courseTable->begin();
	for(; itc != m_courseTable->end(); ++itc)
		courseList.insert(*itc);
}
