/*
 * Course.cpp
 *
 *  Created on: Jan 16, 2015
 *      Author: CHEN Tianwen
 */

#include <cmath>
#include "Course.h"
using namespace std;


// TODO: 
// ========================================================================
// Implementation of class Course
// ========================================================================

const int Course::COURSE_CODE_MIN_LEN = 7;
const int Course::COURSE_CODE_MAX_LEN = 9;
const int Course::COURSE_CODE_ALPHABET_LEN = 4;
const int Course::COURSE_NAME_MIN_LEN = 1;
const int Course::COURSE_NAME_MAX_LEN = 50;
const int Course::COURSE_CREDIT_LOWER_BOUND = 0;
const int Course::COURSE_CREDIT_UPPER_BOUND = 5;


Course::Course(const string& code, const string& name, int credit)
	:m_courseCode(code),
	 m_courseName(name),
	 m_credit(credit)
{
}

// return the attributes
const string& Course::getCourseCode() const
{
	return m_courseCode;
}

const string& Course::getCourseName() const
{
	return m_courseName;
}

int Course::getCourseCredit() const
{
	return m_credit;
}

// set the attributes
// return true if successful to set
bool Course::setCourse(const string& name, int credit)
{
	if(isValidCourseName(name) && isValidCourseCredit(credit))
	{
		m_courseName = name;
		m_credit = credit;
		return true;
	}
	return false;
}

bool Course::setCourseName(const string& name)
{
	if(isValidCourseName(name))
	{
		m_courseName = name;
		return true;
	}
	return false;
}

// return true if successful to set
bool Course::setCourseCredit(int credit)
{
	if(isValidCourseCredit(credit))
	{
		m_credit = credit;
		return true;
	}
	return false;
}

// hash function
// return 0 to (num_bucket)
unsigned int Course::hash(const Course& course, unsigned int num_bucket)
{
	unsigned int hashValue = 0;
	int value;
	if(num_bucket > 0)
	{
		for(int i=0; i<course.m_courseCode.length(); ++i)
		{
			if(course.m_courseCode[i] >= 'A' && course.m_courseCode[i] <= 'Z' )
				value = course.m_courseCode[i] - 'A' + 10;
			else
				value = course.m_courseCode[i] - '0';
			hashValue = static_cast<int>(hashValue + value * pow(static_cast<double>(36 % num_bucket), static_cast<double>(i))) % num_bucket;
		}
	}
	return hashValue;
}

// return true if it is a valid credit
bool Course::isValidCourseCode(const string& code)
{
	if(code.length() >= COURSE_CODE_MIN_LEN && code.length() <= COURSE_CODE_MAX_LEN)
	{
		// check the first four chars
		for(int i=0; i<COURSE_CODE_ALPHABET_LEN; ++i)
			if(code[i] < 'A' || code[i] > 'Z')
				return false;
		// check the rest of chars
		for(int i=COURSE_CODE_ALPHABET_LEN; i<code.length(); ++i)
			if((code[i] < 'A' || code[i] > 'Z')
			   && (code[i] < '0' || code[i] > '9'))
				return false;
		return true;
	}
	return false;
} 

// return true if it is valid
bool Course::isValidCourseName(const string& name)
{
	return name.length() >= COURSE_NAME_MIN_LEN && name.length() <= COURSE_NAME_MAX_LEN;
}


bool Course::isValidCourseCredit(int credit)
{
	return credit >= COURSE_CREDIT_LOWER_BOUND && credit <= COURSE_CREDIT_UPPER_BOUND;
}


// define a "strict weak ordering" over the course code
bool operator<(const Course& c1, const Course& c2)
{
	return c1.m_courseCode < c2.m_courseCode;
}
