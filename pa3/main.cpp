/*
 * test.cpp
 *
 *  Created on: Jan 14, 2015
 *      Author: cspeter
 */

 
#include "utility.h"
#include "Pair.h"
#include "Student.h"
#include "Course.h"
#include "DoublyLinkedList.h"
#include "HashTable.h"
#include "RegistrationManager.h"

#include <iostream>
#include <string>
#include <cassert>
#include <vector>
using namespace std;

// Function declaration of all test cases
void print_student_list(const DoublyLinkedList<Student>& studentlist);
void print_course_list(const DoublyLinkedList<Course>& courselist);
void test_utility();
void test_pair();
void test_dblist();
void test_hashtable();
void test_student();
void test_course();
void test_registrationManager_student();
void test_registrationManager_course();
void test_all();


int main() {
	test_all();
	return 0;
}

void test_all() {

	// Suggestion:
	//    Please toggle the test cases below

	// All test cases are written using assert
	//    No text output


	test_utility();
	cout << "Pass test_utility()" << endl;	
	test_pair();
	cout << "Pass test_pair()" << endl;
	test_dblist();
	cout << "Pass test_dblist()" << endl;
	test_hashtable();
	cout << "Pass test_hashtable()" << endl;
	test_student();
	cout << "Pass test_student()" << endl;
	test_course();
	cout << "Pass test_course()" << endl;

	test_registrationManager_student();
	cout << "Pass test_registrationManager_student()" << endl;
	test_registrationManager_course();
	cout << "Pass test_registrationManager_course()" << endl ;
	
}


//
// Given:
// Implementation of all test cases
//

void test_utility() {
	string *sp = new string("Hello World");
	SAFE_DEL(sp);
	assert(sp == NULL);

	int *ip = new int[5];
	SAFE_DEL_ARRAY (ip);
	assert(ip == NULL );
}

void test_pair() {

	Pair<int, string> p;
	assert( p.getFirst() == 0 );
	assert( p.getSecond() == "");

	Pair<int, string> q(6, "");
	assert( q.getFirst() == 6 );
	assert( q.getSecond() == "" );

	p.set(4, "World");
	assert( p.getFirst() == 4 );
	assert( p.getSecond() == "World" );

	q.setFirst(5);
	q.setSecond("Hello");
	assert( q.getFirst() == 5);
	assert( q.getSecond() == "Hello");

	assert( p < q );

	Pair<int, string> pcopy = p;

	assert( p == pcopy);

}


void test_dblist() {

	DoublyLinkedList<string> list;

	// Usage of typedef - Simplify a declaration:
	// In this example,
	//    StringIterator is equivalent to
	//    DoublyLinkedList<string>::Iterator

	typedef DoublyLinkedList<string>::Iterator StringIterator;
	StringIterator itr, insertAnn;

	// Sample usage of iterator
	// itr = list.begin();
	// while ( itr != list.end() ) {
	//	cout << *itr << " " ;
	//	itr++;
	// }
	// cout << endl;

	list.insert("Peter");
	list.insert("Jason");
	list.insert("King");
	list.insert("Peter");
	list.insert("Rex");
	list.insert("Leo");
	list.insert("Peter");


	insertAnn = list.insert("Ann");
	assert ( *insertAnn == "Ann" );
	itr = list.begin();
	assert( *itr == "Ann" );
	itr = list.search("Peter");
	assert( itr != list.end() ); // found
	itr = list.search("peter");
	assert( itr == list.end() ); // not found


	// Usage of searchRange + deleteElem
	// Delete all "Peter" from the doublyLinkedList
	Pair<StringIterator, StringIterator> result = list.searchRange("Peter");
	StringIterator firstPeter = result.getFirst();
	StringIterator lastPeter = result.getSecond();
	while ( firstPeter != lastPeter ) {
		list.deleteElem(firstPeter); // iterator will be moved by deleteElem
	}

	itr = list.search("Peter");
	assert( itr == list.end() );  // not found

}

unsigned int simpleHashFunc (const int& input, unsigned int bucketSize) {
	return input % bucketSize ;
}

void test_hashtable() {
	const int bucketSize = 5;
	HashTable<int> hTable(bucketSize, simpleHashFunc);
	hTable.insert(1);
	hTable.insert(11);
	hTable.insert(2);
	hTable.insert(21);
	hTable.insert(31);

	assert ( simpleHashFunc(1, bucketSize) == 1 );
	assert ( simpleHashFunc(11, bucketSize) == 1 );
	assert ( simpleHashFunc(21, bucketSize) == 1 );
	assert ( simpleHashFunc(31, bucketSize) == 1 );
	assert ( simpleHashFunc(2, bucketSize) == 2 );

	//Hint: use hTable.print() to debug
	//
	//Expected result:
	//
	//Hash Buckets = 0:
	//Empty
	//Hash Buckets = 1:
	//1 11 21 31
	//Hash Buckets = 2:
	//2
	//Hash Buckets = 3:
	//Empty
	//Hash Buckets = 4:
	//Empty

	assert( hTable.size() == 5 );

	HashTable<int>::Iterator itr;

	itr = hTable.search(50);
	assert( itr == hTable.end() ); // 50 is not found

	itr = hTable.search(21);
	assert( itr != hTable.end() );  // 21 is found

	hTable.deleteElem( itr ) ; // delete 21
	itr = hTable.search(21);
	assert( itr == hTable.end() ) ; // 21 is not found

	assert( hTable.size() == 4 ) ; // one item is removed

}

void test_student() {

	// False validity checking
	assert( Student::isValidStudentID("0xxxyyyy") == false );
	assert( Student::isValidStudentID("012345") == false );
	assert( Student::isValidStudentID("123456789") == false );
	assert( Student::isValidStudentName("") == false );
	assert( Student::isValidStudentName("It is a very very very very very very very long name with more than 32 characters") == false );
	assert( Student::isValidStudentYear(6) == false );
	assert( Student::isValidStudentYear(-1) == false );

	// True validity checking
	assert( Student::isValidStudentID("10234567") == true );
	assert( Student::isValidStudentID("99999999") == true );
	assert( Student::isValidStudentName("CY Leung") == true );
	assert( Student::isValidStudentYear(1) == true );
	assert( Student::isValidStudentYear(4) == true );


	Student testStud("10000000", "Peter", 3, Student::MALE);

	// validity of getter functions
	assert( testStud.getStudentID() == "10000000" );
	assert( testStud.getStudentName() == "Peter" );
	assert( testStud.getYear() == 3 );
	assert( testStud.getGender() == Student::MALE );

	// The hashing result
	const unsigned int numBucket = 29;
	unsigned int expectedHashResult ;

	expectedHashResult = 1;
	assert( Student::hash(testStud, numBucket) == expectedHashResult );

	Student otherStud("30000000", "April", 1, Student::FEMALE);
	expectedHashResult = 3;
	assert( Student::hash(otherStud, numBucket) == expectedHashResult );

	Student generalStud("12345678", "General", 5, Student::MALE);
	expectedHashResult = 23; 	// Google: 87654321 % 29
	assert( Student::hash(generalStud, numBucket) == expectedHashResult );

}

void test_course() {

	// False validity checking
	assert( Course::isValidCourseCode("comp2012") == false ); // lowercase letters - failed
	assert( Course::isValidCourseCode("COMP12") == false ); // too short - failed
	assert( Course::isValidCourseCode("COMP59999Y") == false ); // too long - failed
	assert( Course::isValidCourseCode("POMC@2345") == false ); // invalid char @

	assert( Course::isValidCourseCredit(-1) == false ); // -1 credit - failed
	assert( Course::isValidCourseCredit(10000) == false ); // 10000 credit - failed

	assert( Course::isValidCourseName("") == false ); // noname - failed

	string tooLongName = "Too long, didn't read; ";
	for (int i=0; i<5; i++)
		tooLongName += "Too long, didn't read; ";

	assert( tooLongName.size() > 50 );
	assert( Course::isValidCourseName(tooLongName) == false ) ; // failed, the given name is too long

	// True validity checking
	assert( Course::isValidCourseCode("COMP2012") == true );
	assert( Course::isValidCourseCode("COMP2012H") == true );
	assert( Course::isValidCourseCode("COMP104") == true );
	assert( Course::isValidCourseCredit(0) == true );
	assert( Course::isValidCourseCredit(5) == true );
	assert( Course::isValidCourseName("Object Oriented Programming and Data Structures") == true );


	Course testCourse("COMP2012", "Object Oriented Programming and Data Structures", 4);

	// validity of getter functions
	assert( testCourse.getCourseCode() == "COMP2012" );
	assert( testCourse.getCourseName() == "Object Oriented Programming and Data Structures");
	assert( testCourse.getCourseCredit() == 4 );

	const unsigned int numBucket = 17;
	// Google: (12+24*36+22*36^2+25*36^3+2*36^4+0*36^5+1*36^6+2*36^7)%17
	unsigned int expectedHashResult = 3;
	// cout << Course::hash(testCourse, numBucket) << endl ;
	assert( Course::hash(testCourse, numBucket) == expectedHashResult );
}



void test_registrationManager_course() {

	const int student_bucket = 29;
	const int course_bucket = 17;
	RegistrationManager manager(student_bucket, course_bucket);
	bool result;

	Course allCourses[4];

	allCourses[0] = ( Course("COMP2011", "Introduction to Object-oriented Programming", 4 ));
	allCourses[1] = ( Course("COMP2012", "Object-Oriented Programming and Data Structures", 4 ));
	allCourses[2] = ( Course("COMP3021", "Java Programming", 3 ));
	allCourses[3] = ( Course("COMP2011", "Duplicated", 4 ));

	assert( manager.newCourse(allCourses[0]) == true );
	assert( manager.newCourse(allCourses[2]) == true );
	assert( manager.newCourse(allCourses[1]) == true );
	assert( manager.newCourse(allCourses[3]) == false );

	// Testing: manager.modifyCourse
	Course modifySuccess("COMP2011", "Intro to OOP", 4 );
	assert(manager.modifyCourse(modifySuccess) == true );
	Course modifyFail("COMPXXX", "Dummy Course", 1);
	assert( manager.modifyCourse(modifyFail) == false);


	// Testing: manager.deleteCourse
	assert( manager.deleteCourse("COMPXXX") == false  );
	assert( manager.deleteCourse("COMP3021") == true );

	// Testing: isCourseExist + Retrieve
	string queryCode;
	Course courseRetrived;

	queryCode = "COMPXXX";
	assert( manager.isCourseExist(queryCode) == false   );
	assert( manager.retrieveCourse(queryCode, courseRetrived) == false );

	queryCode = "COMP2012";
	assert( manager.isCourseExist(queryCode) == true );
	assert( manager.retrieveCourse(queryCode, courseRetrived) == true );
	assert( courseRetrived.getCourseCode() == queryCode );
	assert( courseRetrived.getCourseName() == "Object-Oriented Programming and Data Structures");
	assert( courseRetrived.getCourseCredit() == 4);

	DoublyLinkedList<Course> courselist;
	manager.getAllCourses (courselist);
	assert( courselist.size() == 2 );

	DoublyLinkedList<Course>::Iterator itr = courselist.begin();
	assert( (*itr).getCourseCode() == "COMP2011"  );
	assert( (*itr).getCourseName() == "Intro to OOP"  );
	assert( (*itr).getCourseCredit() == 4  );

	itr++;

	assert( (*itr).getCourseCode() == "COMP2012"  );
	assert( (*itr).getCourseName() == "Object-Oriented Programming and Data Structures"  );
	assert( (*itr).getCourseCredit() == 4  );

	// Testing: getAllCourses
	//cout << "Remaining courses:" << endl ;
	//DoublyLinkedList<Course> courselist;
	//manager.getAllCourses (courselist);
	//print_course_list(courselist);
}

void test_registrationManager_student() {

	const int studentBucket = 29;
	const int courseBucket = 17;
	RegistrationManager manager(studentBucket, courseBucket);
	bool result ;

	Student allStudents[4];
	allStudents[0] = Student("12345678", "Peter Chung", 4, Student::MALE);
	allStudents[1] = Student("13141918", "Ann Chan", 3, Student::FEMALE);
	allStudents[2] = Student("20001000", "Ann Chan", 4, Student::FEMALE);
	allStudents[3] = Student("12345678", "Duplicated", 3, Student::MALE);

	assert( manager.newStudent(allStudents[0]) == true );
	assert( manager.newStudent(allStudents[1]) == true );
	assert( manager.newStudent(allStudents[2]) == true );
	assert( manager.newStudent(allStudents[3]) == false );


	// Testing: manager.modifyStudent
	Student modifySuccess("12345678", "Peter Pan", 2, Student::MALE );
	assert( manager.modifyStudent(modifySuccess) == true );
	Student modifyFail("00000001", "Peter Chung", 1, Student::FEMALE);
	assert( manager.modifyStudent(modifyFail) == false );

	// Testing: manager.deleteStudent
	assert( manager.deleteStudent("00000001") == false );
	assert( manager.deleteStudent("20001000") == true );

	// Testing: isStudentExist + Retrieve
	string querySID ;
	Student studRetrived;

	querySID = "00000001";  // Failed to retrieve
	assert( manager.isStudentExist(querySID) == false );
	assert( manager.retrieveStudent(querySID, studRetrived) == false );


	querySID = "12345678";  // Success
	assert( manager.isStudentExist(querySID) == true );
	assert( manager.retrieveStudent(querySID, studRetrived) == true );
	assert( studRetrived.getStudentID() == querySID ) ;
	assert( studRetrived.getStudentName() == "Peter Pan" );
	assert( studRetrived.getGender() == Student::MALE );


	DoublyLinkedList<Student> studentlist;
	manager.getAllStudents(studentlist);

	assert( studentlist.size() == 2 );

	DoublyLinkedList<Student>::Iterator itr = studentlist.begin();
	assert( (*itr).getStudentID() == "12345678" );
	assert( (*itr).getStudentName() == "Peter Pan" );
	assert( (*itr).getYear() == 2 );
	assert( (*itr).getGender() == Student::MALE );
	itr++;
	assert( (*itr).getStudentID() == "13141918" );
	assert( (*itr).getStudentName() == "Ann Chan" );
	assert( (*itr).getYear() == 3 );
	assert( (*itr).getGender() == Student::FEMALE );

	// Printing: getAllStudents
	//cout << "Remaining students:" << endl ;
	//DoublyLinkedList<Student> studentlist;
	//manager.getAllStudents (studentlist);
	//print_student_list(studentlist);
}


void print_course_list(const DoublyLinkedList<Course>& courselist) {

	DoublyLinkedList<Course>::ConstIterator itr;
	for (itr = courselist.begin(); itr!=courselist.end(); itr++) {
		const Course& ref = (*itr);

		cout << ref.getCourseCode() << ", "
             << ref.getCourseCredit() << ", "
             << ref.getCourseName() << endl ;

	}
}

void print_student_list(const DoublyLinkedList<Student>& studentlist) {

	DoublyLinkedList<Student>::ConstIterator itr;
	for (itr = studentlist.begin(); itr!=studentlist.end(); itr++) {
		const Student& ref = (*itr);
		cout << ref.getStudentID() << ", "
		     << ref.getStudentName() << ", "
		     << ref.getYear() << ", "
		     << (ref.getGender()==Student::MALE ? "Male" : "Female") << endl;
	}
}
