/*
 * Student.cpp
 *
 *  Created on: Jan 16, 2015
 *      Author: cspeter
 */

#include <cmath>
#include "Student.h"
using namespace std;

// TODO: 
// ========================================================================
// Implementation of class Student
// ========================================================================

const int Student::STUDENT_ID_LEN = 8;
const int Student::STUDENT_NAME_MAX_LEN = 32;
const int Student::STUDENT_NAME_MIN_LEN = 1;
const int Student::STUDENT_YEAR_LOWER_BOUND = 0;
const int Student::STUDENT_YEAR_UPPER_BOUND = 5;

Student::Student(const string& id, const string& name, int year, GENDER gender)
	:m_studentID(id),
	 m_studentName(name),
	 m_year(year),
	 m_gender(gender)
{
}


// return the attributes
const string& Student::getStudentID() const
{
	return m_studentID;
}

const string& Student::getStudentName() const
{
	return m_studentName;
}

int Student::getYear() const
{
	return m_year;
}


Student::GENDER Student::getGender() const
{
	return m_gender;
}


// set the attributes
// return true if successful to set
bool Student::setStudent(const string& name, int year, GENDER gender)
{
	if(isValidStudentName(name) && isValidStudentYear(year))
	{
		m_studentName = name;
		m_year = year;
		m_gender = gender;
		return true;
	}
	return false;
}

// return true if successful to set
bool Student::setStudentName(const string& name)
{
	if(isValidStudentName(name))
	{
		m_studentName = name;
		return true;
	}
	return false;
}

bool Student::setStudentYear(int year)
{
	if(isValidStudentYear(year))
	{
		m_year = year;
		return true;
	}
	return false;
}

void Student::setStudentGender(GENDER gender)
{
	m_gender = gender;
}


// hash function
// return 0 to (num_bucket)
unsigned int Student::hash(const Student& stu, unsigned int num_bucket)
{
	unsigned int hashValue = 0;
	if(num_bucket > 0)
	{
		for(int i=0; i<stu.m_studentID.length(); ++i)
			hashValue = static_cast<int>(hashValue +
										 (stu.m_studentID[i] - '0') * pow(10.0, static_cast<double>(i))) % num_bucket;
	}
	return hashValue;
}

bool Student::isValidStudentID(const string& id)
{
	if(id.length() == STUDENT_ID_LEN)
	{
		for(int i=0; i<STUDENT_ID_LEN; ++i)
			if(id[i] < '0' || id[i] > '9')
				return false;
		return true;
	}
	return false;
}

// return true if it is valid
bool Student::isValidStudentName(const string& name)
{
	return name.length() >= STUDENT_NAME_MIN_LEN && name.length() <= STUDENT_NAME_MAX_LEN;
}

// return true if it is a valid year
bool Student::isValidStudentYear(int year)
{
	return year >= STUDENT_YEAR_LOWER_BOUND && year <= STUDENT_YEAR_UPPER_BOUND;
}


// define a "strict weak ordering" over student id
bool operator<(const Student& s1, const Student& s2)
{
	return s1.m_studentID < s2.m_studentID;
}

