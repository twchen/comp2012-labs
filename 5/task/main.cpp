/****************************************/
//  COMP2012 2015S Lab05 
//  A simple widget to draw rectangles
//  main.cpp
/****************************************/

#include "my_widget.h"

#include <qapplication.h>

int main(int argc, char *argv[])
{
   QApplication app(argc, argv);

   MyWidget* widget = new MyWidget();
   app.setMainWidget(widget);
   widget->show();

   return app.exec();
}

