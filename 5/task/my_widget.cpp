#include "my_widget.h" 
#include <qpen.h>
#include <qbrush.h>
#include <iostream>

using namespace std;

/************************************/
//  Constructor & Destructor
/************************************/

MyWidget::MyWidget(QWidget* parent, const char* name) : QWidget(parent, name) {

   // create the QPainter for the widget
   painter = new QPainter(this);   

   // create a pixmap image
   image = new QPixmap(400, 300);
   // fill the image with white color
   image->fill(white);

   // set the margin size
   margin_x = 50;
   margin_y = 50;

   // create the push button to save the image into a bitmap file
   saveButton = new QPushButton("Save Image", this);
   QObject::connect(saveButton, SIGNAL(clicked()), this, SLOT(saveImage()));
   saveButton->setGeometry(150, 370, 200, 30);

   // set the size of the window
   resize(500, 450);
}

MyWidget::~MyWidget(){

   delete painter;
   delete image;
   delete saveButton;
}

/************************************/
//  Mouse Event Handlers
/************************************/

// Mouse Press Event Handler 
void MyWidget::mousePressEvent(QMouseEvent * e) {

   // check if the button that caused the event the right button of the mouse
   if ( e->button() == Qt::RightButton ) {

      // copy the existing pixmap image back to the widget
      paintImage();

      // create a QPen object with black line color, width size of 5 and solid line style 
      QPen pen(black, 5, Qt::SolidLine);

      // create a QBrush object with blue fill color, and solid pattern fill style
      QBrush brush(blue, Qt::SolidPattern);

      // close the active painter
      if ( painter->isActive() )
         painter->end();

      // begins painting the widget
      painter->begin(this);
      painter->setPen(pen); // set the pen of the painter
      painter->setBrush(brush); // set the brush of the painter
      painter->setClipRect(margin_x, margin_y, 400, 300); // enable drawing with a rectangular area only
      painter->drawRect(e->x(), e->y(), 80, 120); // draw a rectangle of size 80 by 120
      painter->end();

      // begins painting the pixmap image
      painter->begin(image);
      painter->setPen(pen); // set the pen of the painter
      painter->setBrush(brush); // set the brush of the painter
      painter->drawRect(e->x() - margin_x, e->y() - margin_y, 80, 120); // draw a rectangle of size 80 by 120
      painter->end();

   } else if ( e->button() == Qt::LeftButton ) {
      // TODO: implement the actions for mouse left button press
	   prev_x = e->x();
	   prev_y = e->y();
   }
}

// Mouse Move Event Handler 
// TODO: implement the mouse move event handler
void MyWidget::mouseMoveEvent(QMouseEvent* e)
{
	if(e->state() == Qt::LeftButton){
		// Note that the returned value of button() is always NoButton for mouse move events.
		// The only word I want to say is Nima ......
		paintImage();
	
		QPen pen(Qt::black, 5, Qt::SolidLine);

		QBrush brush(Qt::blue, Qt::SolidPattern);

		if(painter->isActive())
			painter->end();

		painter->begin(this);
		painter->setPen(pen);
		painter->setBrush(brush);
		painter->setClipRect(margin_x, margin_y, 400, 300);
		// press event must happen before move event, so prev_x and prev_y must be initialized;
		painter->drawRect(prev_x, prev_y, e->x() - prev_x, e->y() - prev_y);
		painter->end();
	}
}

// Mouse Release Event Handler 
// TODO: implement the mouse release event handler
void MyWidget::mouseReleaseEvent(QMouseEvent* e)
{
	if(e->button() == Qt::LeftButton){
		QPen pen(Qt::black, 5, Qt::SolidLine);
		QBrush brush(Qt::blue, Qt::SolidPattern);

		if(painter->isActive())
			painter->end();

		// draw on the image
		painter->begin(image);
		painter->setPen(pen);
		painter->setBrush(brush);
		painter->drawRect(prev_x - margin_x, prev_y - margin_y, e->x() - prev_x, e->y() - prev_y);
		painter->end();

		paintImage();
	}
}

/************************************/
//  Paint Event Handler
/************************************/
void MyWidget::paintEvent(QPaintEvent *) {

   // copy the existing pixmap image back to the widget
   paintImage();

}

/************************************/
//  Member Functions 
/************************************/

// paintImage() function is to copy the existing pixmap image back to the widget
void MyWidget::paintImage() {

   // close the active painter
   if ( painter->isActive() ) {
      painter->end();
   }
   // begin drawing on the widget
   painter->begin(this);

   // draw the pixmap from the image QPixmap object to the widget
   if ( !image->isNull() ) {
      painter->drawPixmap(margin_x, margin_y, (*image));
   }

   painter->end();
}

// paintImage() function is to save the pixmap image into a bitmap image file, named "temp.bmp" 
void MyWidget::saveImage() {

   image->save("temp.bmp", "BMP");
}
