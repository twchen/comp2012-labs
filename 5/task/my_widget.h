/****************************************/
//  COMP2012 2015S Lab05 
//  A simple widget to draw rectangles
//  my_widget.h
/****************************************/

#ifndef _MY_WIDGET_H
#define _MY_WIDGET_H

#include <qapplication.h>
#include <qpushbutton.h>
#include <qpainter.h>
#include <qpixmap.h>

class MyWidget : public QWidget
{
	Q_OBJECT

public:
	MyWidget(QWidget* parent = 0, const char* name = 0);
	~MyWidget();

	// to render the image onto the widget
	void paintImage();

	/* paint handler */
	void paintEvent(QPaintEvent* e);

	/* mouse event handler */
	void mousePressEvent(QMouseEvent* e);

	// TODO: Add the mouse move and mouse release event handlers
	void mouseMoveEvent(QMouseEvent* e);
	void mouseReleaseEvent(QMouseEvent* e);


public slots: 
	// to save the image into a bitmap file
	void saveImage();

private:
	QPainter* painter;
	QPixmap* image;

	QPushButton* saveButton;
     
	// for storing the size of the margin to the top and the left edge of the window 
	int margin_x, margin_y;
      
	// for storing the mouse press position
	int prev_x, prev_y;
};

#endif
